//
//  OrderDetailVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 25/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class OrderDetailVC: UIViewController {
    
    @IBOutlet var Scrollview : UIScrollView!
    var orderDic : NSDictionary!
    var itemtotalprice : Float = 0.0
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Order detail"
        self.CustumDesignMethod()

    }

    
    func CustumDesignMethod() -> Void {
       let width1 = Scrollview.frame.size.width/20
        
        let AddressView = UIView(frame: CGRect(x: 0, y: 10, width: Scrollview.frame.size.width, height: 165))
        AddressView.backgroundColor = UIColor.white
        Scrollview.addSubview(AddressView)
        let addTitle = UILabel(frame: CGRect(x: width1, y: 0, width: AddressView.frame.size.width - width1, height: 40))
        addTitle.text = "DELIVERY TO"
        addTitle.font = UIFont.systemFont(ofSize: 14.0)
        AddressView.addSubview(addTitle)
        
        let areaLbl = UILabel(frame: CGRect(x: width1, y: addTitle.frame.origin.y + addTitle.frame.size.height - 2, width: AddressView.frame.size.width - width1, height: 25))
        let area = CoredataClass().getAreaFromAreaId(orderDic.object(forKey: "area_id") as! String) as Area
        areaLbl.text = "Area : \(area.area_name!)"
        areaLbl.font = UIFont.systemFont(ofSize: 9.0)
        AddressView.addSubview(areaLbl)
        
        let blockLbl = UILabel(frame: CGRect(x: width1, y: areaLbl.frame.origin.y + areaLbl.frame.size.height, width: AddressView.frame.size.width - width1, height: 25))
        blockLbl.text = "Block No.: \(orderDic.object(forKey: "block") as! String)"
        blockLbl.font = UIFont.systemFont(ofSize: 9.0)
        AddressView.addSubview(blockLbl)
        
        let streetLbl = UILabel(frame: CGRect(x: width1, y: blockLbl.frame.origin.y + blockLbl.frame.size.height, width: AddressView.frame.size.width - width1, height: 25))
        streetLbl.text = "Street No.: \(orderDic.object(forKey: "street") as! String)"
        streetLbl.font = UIFont.systemFont(ofSize: 9.0)
        AddressView.addSubview(streetLbl)
        
        let houseLbl = UILabel(frame: CGRect(x: width1, y: streetLbl.frame.origin.y + streetLbl.frame.size.height, width: AddressView.frame.size.width - width1, height: 25))
        houseLbl.text = "House No.: \(orderDic.object(forKey: "house_no") as! String)"
        houseLbl.font = UIFont.systemFont(ofSize: 9.0)
        AddressView.addSubview(houseLbl)
        
        let extra_infoLbl = UILabel(frame: CGRect(x: width1, y: houseLbl.frame.origin.y + houseLbl.frame.size.height, width: AddressView.frame.size.width - width1, height: 25))
        extra_infoLbl.text = "Other delivery information : \(orderDic.object(forKey: "extra_delivery_info") as! String)"
        extra_infoLbl.font = UIFont.systemFont(ofSize: 9.0)
        AddressView.addSubview(extra_infoLbl)

        self.addLine(AddressView)
        
        /////////////////////////Order detail//////////
        let itemArr = orderDic.object(forKey: "orderarray") as! NSArray
        
        let bottomView = UIView(frame: CGRect(x: 0, y: AddressView.frame.origin.y + AddressView.frame.size.height + 10, width: Scrollview.frame.size.width, height: CGFloat(40*itemArr.count + 75)))
        bottomView.backgroundColor = UIColor.white
        Scrollview.addSubview(bottomView)
        let orderTItle = UILabel(frame: CGRect(x: width1, y: 0, width: AddressView.frame.size.width - width1, height: 50))
        orderTItle.text = "ORDER DETAIL"
        orderTItle.font = UIFont.systemFont(ofSize:14.0)
        bottomView.addSubview(orderTItle)
        
        let width: CGFloat = (bottomView.frame.size.width) / 5
        let titelview = UIView(frame: CGRect(x: 0, y: orderTItle.frame.origin.y + orderTItle.frame.size.height, width: bottomView.frame.size.width, height: 25))
        bottomView.addSubview(titelview)
        let QTYlbl = UILabel(frame: CGRect(x: width1, y: 0, width: width, height: 25))
        QTYlbl.text = "QTY"
        QTYlbl.font = UIFont.systemFont(ofSize: 12.0)
        titelview.addSubview(QTYlbl)
        let ItemLbltitel = UILabel(frame: CGRect(x: width , y: 0, width: width * 3 - width1, height: 25))
        ItemLbltitel.text = "ITEM"
        ItemLbltitel.font = UIFont.systemFont(ofSize: 12.0)
        ItemLbltitel.textAlignment = .left
        titelview.addSubview(ItemLbltitel)
        let Pricelbl = UILabel(frame: CGRect(x: width * 4 - width1, y: 0, width: width, height: 25))
        Pricelbl.text = "PRICE"
        Pricelbl.font = UIFont.systemFont(ofSize: 12.0)
        Pricelbl.textAlignment = .right
        titelview.addSubview(Pricelbl)
        
        ////////////////////// Item content view ///////////////
        let ItemContentView = UIView(frame: CGRect(x: 0, y: titelview.frame.origin.y + titelview.frame.size.height, width: bottomView.frame.size.width, height: CGFloat(40*itemArr.count)))
        bottomView.addSubview(ItemContentView)
//        itemtotalprice = 0.0
        for i in 0..<itemArr.count {
            let itemdic = itemArr.object(at: i) as! NSDictionary
            
            let view1 = UIView(frame: CGRect(x: 0, y: CGFloat(i*40), width: ItemContentView.frame.size.width, height: 40))
            ItemContentView.addSubview(view1)
            let quentityLbl = UILabel(frame: CGRect(x: width1, y: 0, width: width, height: 40))
            quentityLbl.text = "\(itemdic.object(forKey: "qty") as! String)X"
            quentityLbl.font = UIFont.systemFont(ofSize: 9.0)
            view1.addSubview(quentityLbl)
            
            let itemLbl = UILabel(frame: CGRect(x: CGFloat(width), y: 0, width: CGFloat(ItemContentView.frame.size.width - 2 * width), height: 40))
            itemLbl.textAlignment = .left
            itemLbl.numberOfLines = 0
            itemLbl.text = "\(itemdic.object(forKey: "product_name")!)"
            
            itemLbl.font = UIFont.systemFont(ofSize: 9.0)
            view1.addSubview(itemLbl)
            
            let price = Float(itemdic.object(forKey: "qty") as! String)!*Float(itemdic.object(forKey: "product_price") as! String)!
            let priceLbl = UILabel(frame: CGRect(x: CGFloat(width * 4 - width1), y: 0, width: width, height: 40))
            priceLbl.text = String(format: "KD %0.2f", price)//"KD \(price)"
            priceLbl.textAlignment = .right
            priceLbl.font = UIFont.systemFont(ofSize: CGFloat(9.0))
            view1.addSubview(priceLbl)
            
            itemtotalprice = itemtotalprice + price;
        }
        self.addLine(bottomView)
        
        //////////////////////  Price labels///////////
        
        let subtotalLbl = UILabel(frame: CGRect(x: CGFloat(width1 / 2), y: CGFloat(bottomView.frame.origin.y + bottomView.frame.size.height + 10), width: CGFloat(Scrollview.frame.size.width / 2 - width1 / 2), height: CGFloat(20)))
        subtotalLbl.text = "Subtotal"
        subtotalLbl.font = UIFont.systemFont(ofSize: CGFloat(12.0))
        Scrollview.addSubview(subtotalLbl)
        
        let subtotalPrice = UILabel(frame: CGRect(x: CGFloat(Scrollview.frame.size.width / 2), y: CGFloat(subtotalLbl.frame.origin.y), width: CGFloat(subtotalLbl.frame.size.width), height: CGFloat(20)))
        subtotalPrice.text = String(format: "KD %0.2f", itemtotalprice)//"KD \()"
        subtotalPrice.textAlignment = .right
        subtotalPrice.font = UIFont.systemFont(ofSize: CGFloat(12.0))
        Scrollview.addSubview(subtotalPrice)
        
        let deliverylLbl = UILabel(frame: CGRect(x: CGFloat(width1 / 2), y: CGFloat(subtotalPrice.frame.origin.y + subtotalPrice.frame.size.height), width: CGFloat(Scrollview.frame.size.width / 2 - width1 / 2), height: CGFloat(20)))
        deliverylLbl.text = "Delivery fee"
        deliverylLbl.font = UIFont.systemFont(ofSize: CGFloat(12.0))
        Scrollview.addSubview(deliverylLbl)
        let deliveryPrice = UILabel(frame: CGRect(x: CGFloat(Scrollview.frame.size.width / 2), y: CGFloat(deliverylLbl.frame.origin.y), width: CGFloat(deliverylLbl.frame.size.width), height: CGFloat(20)))
        deliveryPrice.text = String(format: "KD %0.2f", Float(orderDic.object(forKey: "delivery_charge") as! String)!)//"KD \(orderDic.object(forKey: "delivery_charge") as! String)"
        deliveryPrice.textAlignment = .right
        deliveryPrice.font = UIFont.systemFont(ofSize: CGFloat(12.0))
        Scrollview.addSubview(deliveryPrice)
        
        let TotallLbl = UILabel(frame: CGRect(x: CGFloat(width1 / 2), y: CGFloat(deliverylLbl.frame.origin.y + deliverylLbl.frame.size.height + 10), width: CGFloat(Scrollview.frame.size.width / 2 - width1 / 2), height: CGFloat(20)))
        TotallLbl.text = "Total Amount"
        TotallLbl.font = UIFont.systemFont(ofSize: CGFloat(14.0))
        Scrollview.addSubview(TotallLbl)
        let TotalPrice = UILabel(frame: CGRect(x: CGFloat(Scrollview.frame.size.width / 2), y: CGFloat(TotallLbl.frame.origin.y), width: CGFloat(TotallLbl.frame.size.width), height: CGFloat(20)))
        TotalPrice.text = String(format: "KD %0.2f", Float(orderDic.object(forKey: "total") as! String)!)//"KD \(orderDic.object(forKey: "total") as! String)"
        TotalPrice.textAlignment = .right
        TotalPrice.font = UIFont.systemFont(ofSize: CGFloat(14.0))
        Scrollview.addSubview(TotalPrice)
        Scrollview.contentSize = CGSize(width: CGFloat(self.view.frame.size.width), height: CGFloat(TotalPrice.frame.origin.y + TotalPrice.frame.size.height + 20))
    }
    
    
    func addLine(_ superView: UIView) {
        let line = UILabel(frame: CGRect(x: 0, y: superView.frame.size.height - 1, width: superView.frame.size.width, height: 2))
        line.backgroundColor = UIColor.lightGray
        superView.addSubview(line)
    }
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
