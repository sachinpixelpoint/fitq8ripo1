//
//  ResetPasswordVC_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 28/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class ResetPasswordVC_ar: UIViewController {
    
    var email_id : String!
    var code : String!
    var is_pssUpdate = false
    @IBOutlet var currentPassTextF : UITextField!
    @IBOutlet var passwordTextF : UITextField!
    @IBOutlet var passwordTextF1 : UITextField!
    @IBOutlet var resetBtn : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "تحديث كلمة المرور"
        if is_pssUpdate == false {
            currentPassTextF.isHidden = true
            passwordTextF1.frame = passwordTextF.frame
            passwordTextF.frame = currentPassTextF.frame
            
            var frame = resetBtn.frame
            frame.origin.y = frame.origin.y-(currentPassTextF.frame.size.height+30)
            resetBtn.frame = frame
        }
    }
    
    func UpdatePassword() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            var url = String()
            var  parameters = Parameters()
            if is_pssUpdate == false {
                parameters = ["apikey":API_KEY,
                              "user_email":email_id as String,
                              "newpassword":passwordTextF.text! as String,
                              "password_salt":code as String,
                ]
                
                url = UPDATE_PASSWORD
            }
            else{
                let usersession = UserSession().getUserSession()
                parameters = ["apikey":API_KEY,
                              "user_id" : usersession.userId! as String,
                              "old_password": currentPassTextF.text! as String,
                              "newpassword": passwordTextF.text! as String
                    
                ]
                url = CHANGE_PASS
            }
            
            Alamofire.request(url, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status")as! Bool == true) {
                        
                        if self.is_pssUpdate == false {
                            var array: [Any]? = self.navigationController?.viewControllers
                            self.navigationController!.popToViewController(array?[1] as! UIViewController, animated: true)
                        }
                        else{
                            self.navigationController!.popToRootViewController(animated: true)
                        }
                    }
                    else{
                        
                        let alert = DXAlertView(title: nil, contentText: jsonObject.object(forKey: "message") as? String, leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                        }
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    
                }
                
            })
        }
        else{
            
            
        }
    }
    
    func checkPasswork() -> Void {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func Updatebtn_method() {
        if((currentPassTextF.text?.isEmpty)! == true  && is_pssUpdate == true){
            
            let alert = DXAlertView(title: nil, contentText: "الرجاء إدخال كلمة المرور الحالية",
            leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.currentPassTextF.becomeFirstResponder()
            }
        }
         if((passwordTextF.text?.isEmpty)! == true || (passwordTextF.text?.characters.count)!<6){
            
            let alert = DXAlertView(title: nil, contentText: "الرجاء إدخال ٦ خانات على الأقل لكلمة المرور ", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.passwordTextF.becomeFirstResponder()
            }
        }
        else if (passwordTextF.text != passwordTextF1.text){
            let alert = DXAlertView(title: nil, contentText: "كلمة المرور غير متطابقة",
            leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
            }
        }
        else{
            self.UpdatePassword()
        }
        
    }
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
