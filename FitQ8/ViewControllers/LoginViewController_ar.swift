//
//  LoginViewController_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 18/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController_ar: UIViewController {

    @IBOutlet var emailtext :UITextField?
    @IBOutlet var passtext :UITextField?
    @IBOutlet var Login:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title="تسجيل دخول"
        // Do any additional setup after loading the view.
        if UserDefaults.standard.bool(forKey: "loginFrom_another") {
            self.tabBarController?.tabBar.isHidden = true
        }
        
    }
    
    @IBAction func LoginBtn(sender : AnyObject){
        if !isValidEmail(testStr:(emailtext?.text)!){
            let alert = DXAlertView(title: nil, contentText: "الرجاء إدخال بريد إلكتروني صحيح", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.emailtext?.becomeFirstResponder()
            }
            
        }else if(passtext?.text?.isEmpty == true){
            self.passtext?.becomeFirstResponder()
            let alert = DXAlertView(title: nil, contentText: "الرجاء إدخال كلمة المرور", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.passtext?.becomeFirstResponder()
            }
            
        }else{
            
            self.signinmethod()
        }
        
    }
    
    
    func signinmethod() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "user_email":(emailtext?.text)! as String,
                              "pass":(passtext?.text)! as String
            ]
            Alamofire.request(SIGN_IN_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status")as! Bool == true) {
                        let dict = jsonObject.object(forKey: "data") as! NSDictionary
                        let Usersession = UserSession()
                        Usersession.userId = dict.object(forKey: "id") as? String
                        Usersession.userName = dict.object(forKey: "fname") as? String
                        Usersession.email = dict.object(forKey: "user_email") as? String
                        Usersession.gender = dict.object(forKey: "gender") as? String
                        Usersession.phoneNo = dict.object(forKey: "contactno") as? String
                        UserSession().SetUserSession(Usersession)
                        if UserDefaults.standard.bool(forKey: "loginFrom_another") {
                            UserDefaults.standard.set(false, forKey: "loginFrom_another")
                            if UserDefaults.standard.bool(forKey: "Forcart") {
                                UserDefaults.standard.set(false, forKey: "Forcart")
                                UserDefaults.standard.set(true, forKey: "login_forCart")
                            }
                            self.dismiss(animated: true, completion: nil)
                        }
                        else{
                            self.navigationController!.popToRootViewController(animated: true)
                        }
                    }
                    else{
                        let alert = DXAlertView(title: nil, contentText: " الرجاء إدخال الأيميل و كلمة المرور ", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                            self.emailtext?.becomeFirstResponder()
                        }
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed_ar, leftButtonTitle: nil, rightButtonTitle: "Ok")
                    alert?.show()
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet_ar, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        /////////// Add left panel button /////
        if UserDefaults.standard.bool(forKey: "loginFrom_another") {
            let btn1 = UIButton(type: .custom)
            btn1.setImage(UIImage(named: "back_white"), for: .normal)
            btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            btn1.addTarget(self, action: #selector(self.Move_to_Back), for: .touchUpInside)
            let item1 = UIBarButtonItem(customView: btn1) as UIBarButtonItem
            self.navigationItem.leftBarButtonItem = item1
        }
        else{
            Utility().AddSidePanel(VC: self)
        }
        
        
        let usersession = UserSession().getUserSession()
        if  usersession.userId != nil {
            if UserDefaults.standard.bool(forKey: "loginFrom_another") {
                UserDefaults.standard.set(false, forKey: "loginFrom_another")
                self.dismiss(animated: false, completion: nil)
            }
            else{
                self.navigationController!.popToRootViewController(animated: false)
            }
        }
        
    }
    
    
    func Move_to_Back() -> Void {
        UserDefaults.standard.set(false, forKey: "loginFrom_another")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func moveToSignUpVC() -> Void {
        self.performSegue(withIdentifier: "signup", sender: self)
        
    }
    
    @IBAction func moveToForgotVC() -> Void{
        self.performSegue(withIdentifier: "forgot", sender: self)
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    @IBAction func BackButton() -> Void {
        self.navigationController!.popViewController(animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



