//
//  ThankYouVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 06/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class ThankYouVC: UIViewController {
    
//    @IBOutlet var thanklbl :UILabel?
    @IBOutlet var continiueBtn : UIButton?
//    @IBOutlet var Orlbl :UILabel?
    @IBOutlet var orderIdLbl : UILabel!
    var order_id : String!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if UserDefaults.standard.object(forKey: lang) as! String == english {
            self.navigationItem.title = "Thank You"
            orderIdLbl.text = "Order Id : \(order_id!)"
        }
        else{
            self.navigationItem.title = "شكراً لك"
            orderIdLbl.text = "رقم الطلب : \(order_id!)"
        }
        
        
        // Do any additional setup after loading the view.
        
        CoredataClass().DeleteAllData("Cart")

    }
    
    
    @IBAction func continiueBtn_Method(_ sender : Any) -> Void {
        UserDefaults.standard.set(0, forKey: SelectedTab)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "revalvc") as! SWRevealViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.set(rootViewController: vc)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /////////// Add left panel button /////
        Utility().AddSidePanel(VC: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
