//
//  SelectAddressVC_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 18/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class SelectAddressVC_ar: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var  AddressTable: UITableView!
    @IBOutlet var norecordLbl : UILabel!
    var  AddressArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "أختر عنوان"
        norecordLbl.text = No_record_ar
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        AddressTable.register(UINib.init(nibName: "addressCell_ar", bundle: nil), forCellReuseIdentifier: "cell")
        norecordLbl.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.MyADD_Method()
    }
    
    func MyADD_Method() -> Void {
        if  Utility().hasInternetConnection() {
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            let user = UserSession().getUserSession()
            
            let parameters = ["apikey":API_KEY,
                              "user_id": user.userId! as String]
            
            Alamofire.request(GET_ADD_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        CoredataClass().DeleteAllData("Address")
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        for i in 0..<dicEntry.count  {
                            let dictCat = dicEntry.object(at: i) as! NSDictionary
                            print(dictCat)
                            
                            let className = NSStringFromClass(Address.self) as String
                            let entity = NSEntityDescription.entity(forEntityName:className, in: moc)!
                            let cat = (NSManagedObject(entity: entity, insertInto: moc) as! Address)
                            cat.safeSetValuesForKeys(with: dictCat as Any? as? [AnyHashable: Any] ?? [:] )
                            
                            do {
                                try moc.save()
                            } catch {
                                print(error)
                            }
                        }
                        self.AddressArray = CoredataClass().getAddressData().mutableCopy() as! NSMutableArray
                        print(self.AddressArray)
                        self.AddressTable.delegate = self
                        self.AddressTable.dataSource = self
                        self.AddressTable.reloadData()
                        if self.AddressArray.count == 0 {
                            self.norecordLbl.isHidden = false
                        }
                        
                    }
                    else{
                        if self.AddressArray.count == 0 {
                            self.norecordLbl.isHidden = false
                        }
                    }
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed_ar, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.MyADD_Method()
                    }
                    
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,
            leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.AddressArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        
        let cell:CustumCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustumCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let add = AddressArray.object(at: indexPath.row) as! Address
        let area = CoredataClass().getAreaFromAreaId(add.area_id!) as Area
        
        cell.addresnamelbl?.text = "\(add.address_name!)"
        cell.arealbl?.text = "\(area.area_name_ar!)"
        cell.blocklbl?.text = "كتلة رقم : \(add.block!)"
        cell.streetlbl?.text = "شارع : \(add.street_num!)"
        cell.houselbl?.text = "منزل / مبنى رقم: \(add.house_num!)"
        cell.phoneNolbl?.text = "رقم الهاتف.: \(add.phone!)"
        
        return cell
        
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        let address = AddressArray.object(at: indexPath.row) as! Address
        let areaId = UserDefaults.standard.object(forKey: area_id) as? String
        let area = CoredataClass().getAreaFromAreaId(areaId! as String) as Area
        if areaId == address.area_id {
            let chackoutVC = self.storyboard?.instantiateViewController(withIdentifier: "chackout") as! CheckoutViewController_ar
            chackoutVC.address = AddressArray.object(at: indexPath.row) as! Address
            self.navigationController?.pushViewController(chackoutVC, animated: true)
        }
        else{
            let str = "Please select address in \(area.area_name_ar! as String)"
            let alert = DXAlertView(title: nil, contentText:str, leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
        }
    }


    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
