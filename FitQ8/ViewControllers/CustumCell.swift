//
//  CustumCell.swift
//  FitQ8
//
//  Created by Manish Kumar on 24/12/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class CustumCell: UITableViewCell {
    
    @IBOutlet weak  var storeName : UILabel?
    @IBOutlet var store_des : UILabel?
    @IBOutlet var btnBranch : UILabel?
    @IBOutlet var storeImage : UIImageView?
    @IBOutlet var backView : UIView?
    @IBOutlet var Rateview : RateView?
    @IBOutlet var RateButton : UIButton?
    @IBOutlet var lineLbl : UILabel?
    
    @IBOutlet var priceLbl : UILabel?
    @IBOutlet var Quentitylbl : UILabel?

    ///////// cart attrubutes
    @IBOutlet var cartIncreaseBtn : UIButton?
    @IBOutlet var cartDecreaeseBtn : UIButton?
    
    ///////
    @IBOutlet var addressTitle : UILabel?
    @IBOutlet var addresnamelbl : UILabel?
    @IBOutlet var arealbl : UILabel?
    @IBOutlet var blocklbl : UILabel?
    @IBOutlet var streetlbl : UILabel?
    @IBOutlet var houselbl : UILabel?
    @IBOutlet var phoneNolbl : UILabel?
    ///////
    @IBOutlet var cancelbtn :UIButton?
    @IBOutlet var editbtn :UIButton?
    
    /////// orders 
    @IBOutlet var order_idLbl : UILabel?
    @IBOutlet var order_dateLbl : UILabel?
    @IBOutlet var order_statusLbl : UILabel?
    @IBOutlet var cancl_reorderBtn : UIButton?
    @IBOutlet var cancl_reorderLbl : UILabel?
    @IBOutlet var cancl_reorderImg : UIImageView?
    @IBOutlet var cancl_reorderView : UIView?
    
    @IBOutlet var LikeButton : UIButton?
    
    @IBOutlet var cat_store : UILabel?
    @IBOutlet var cat_storeBtn : UIButton?
    
}
