//
//  RateViewController_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 19/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class RateViewController_ar: UIViewController {
    
    var store : Store?
    var product : Product?
    
    @IBOutlet var Rate_starLbl : UILabel?
    @IBOutlet var TotalRatinglbl : UILabel?
    @IBOutlet var bottomView : UIView?
    
    @IBOutlet var fiveRateView : UIView?
    @IBOutlet var fourRateView : UIView?
    @IBOutlet var threeRateVIew : UIView?
    @IBOutlet var twoRateVIew : UIView?
    @IBOutlet var oneRateView : UIView?
    @IBOutlet var fiveRatelbl : UILabel?
    @IBOutlet var fourRatelbl : UILabel?
    @IBOutlet var threeRatelbl : UILabel?
    @IBOutlet var twoRatelbl : UILabel?
    @IBOutlet var oneRatelbl : UILabel?
    
    @IBOutlet var StarOne : UIButton!
    @IBOutlet var StarTwo : UIButton!
    @IBOutlet var StarThree : UIButton!
    @IBOutlet var StarFour : UIButton!
    @IBOutlet var StarFive : UIButton!
    @IBOutlet var writeReviewbtn : UIButton!
    var rating = 0
    var  Is_store = false
    var star1_rating : CGFloat = 0
    var star2_rating : CGFloat = 0
    var star3_rating : CGFloat = 0
    var star4_rating : CGFloat = 0
    var star5_rating : CGFloat = 0
    
    var totalRatingCount : CGFloat = 0
    var baseRating : CGFloat = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Is_store = Variables.Is_Store
        if Is_store {
            store = Variables.store

            writeReviewbtn.setTitle("قيم الآن", for: .normal)
            if  store?.ratingdata?.isEmpty == false {
                let datadic = (store?.ratingdata!)?.objectFromJSONString() as! NSDictionary
                print(datadic)
                totalRatingCount = CGFloat((datadic.object(forKey: "totalstar") as! NSString).floatValue)
                baseRating = CGFloat((datadic.object(forKey: "totalrecord") as! NSString).floatValue)
                star1_rating = CGFloat((datadic.object(forKey: "1star") as! NSString).floatValue)
                star2_rating = CGFloat((datadic.object(forKey: "2star") as! NSString).floatValue)
                star3_rating = CGFloat((datadic.object(forKey: "3star") as! NSString).floatValue)
                star4_rating = CGFloat((datadic.object(forKey: "4star") as! NSString).floatValue)
                star5_rating = CGFloat((datadic.object(forKey: "5star") as! NSString).floatValue)
            }
            
            let totalrate = devide(lhs: totalRatingCount, rhs: baseRating)
            Rate_starLbl?.text = String(format: "%0.1f", totalrate)
            TotalRatinglbl?.text = "(بناء على \(Int(baseRating)) التصنيف)"
            self.topVIewDesign()
            
        }
        else{
            product = Variables.product
            
            if product?.rating?.isEmpty == false {
                let datadic = (product?.rating!)?.objectFromJSONString() as! NSDictionary
                totalRatingCount = CGFloat((datadic.object(forKey: "totalstar") as! NSString).floatValue)
                baseRating = CGFloat((datadic.object(forKey: "totalrecord") as! NSString).floatValue)
                star1_rating = CGFloat((datadic.object(forKey: "1star") as! NSString).floatValue)
                star2_rating = CGFloat((datadic.object(forKey: "2star") as! NSString).floatValue)
                star3_rating = CGFloat((datadic.object(forKey: "3star") as! NSString).floatValue)
                star4_rating = CGFloat((datadic.object(forKey: "4star") as! NSString).floatValue)
                star5_rating = CGFloat((datadic.object(forKey: "5star") as! NSString).floatValue)
            }
            
            let totalrate = devide(lhs: totalRatingCount, rhs: baseRating)
            Rate_starLbl?.text = String(format: "%0.1f", totalrate)
            TotalRatinglbl?.text = "(بناء على \(Int(baseRating)) التصنيف)"
            
            self.topVIewDesign()
        }
        
        self.navigationItem.title = "تقييم"
        
        writeReviewbtn.layer.cornerRadius = 10
        writeReviewbtn.isHighlighted = true
        writeReviewbtn.isUserInteractionEnabled = false
    }
    
    //////////// devide function
    func devide(lhs: CGFloat, rhs: CGFloat) -> CGFloat {
        if rhs == 0 {
            return 0
        }
        return lhs/rhs
    }
    
    
    ///////////// Rating design ///////////
    func topVIewDesign() -> Void {
        let width = fiveRateView?.frame.size.width
        
        var frame1 = fiveRateView?.frame
        frame1?.size.width = (devide(lhs: width!, rhs: baseRating))*star5_rating
        frame1?.origin.x = (frame1?.origin.x)!+width!-(frame1?.size.width)!
        self.fiveRateView?.frame = frame1!
        
        var frame2 = fourRateView?.frame
        frame2?.size.width = (devide(lhs: width!, rhs: baseRating))*self.star4_rating
        frame2?.origin.x = (frame2?.origin.x)!+width!-(frame2?.size.width)!
        self.fourRateView?.frame = frame2!
        
        var frame3 = threeRateVIew?.frame
        frame3?.size.width = (devide(lhs: width!, rhs: baseRating))*self.star3_rating
        frame3?.origin.x = (frame3?.origin.x)!+width!-(frame3?.size.width)!
        self.threeRateVIew?.frame = frame3!
        
        var frame4 = twoRateVIew?.frame
        frame4?.size.width = (devide(lhs: width!, rhs: baseRating))*self.star2_rating
        frame4?.origin.x = (frame4?.origin.x)!+width!-(frame4?.size.width)!
        self.twoRateVIew?.frame = frame4!
        
        var frame5 = oneRateView?.frame
        frame5?.size.width = (devide(lhs: width!, rhs: baseRating))*self.star1_rating
        frame5?.origin.x = (frame5?.origin.x)!+width!-(frame5?.size.width)!
        self.oneRateView?.frame = frame5!
        
        var lblframe1 = fiveRatelbl?.frame
        lblframe1?.origin.x = (frame1?.origin.x)!-(lblframe1?.size.width)!-5
        fiveRatelbl?.frame = lblframe1!
        fiveRatelbl?.text = "(\(UInt(star5_rating)))"
        
        var lblframe2 = fourRatelbl?.frame
        lblframe2?.origin.x = (frame2?.origin.x)!-(lblframe2?.size.width)!-5
        fourRatelbl?.frame = lblframe2!
        fourRatelbl?.text = "(\(UInt(star4_rating)))"
        
        var lblframe3 = threeRatelbl?.frame
        lblframe3?.origin.x = (frame3?.origin.x)!-(lblframe3?.size.width)!-5
        threeRatelbl?.frame = lblframe3!
        threeRatelbl?.text = "(\(UInt(star3_rating)))"
        
        var lblframe4 = twoRatelbl?.frame
        lblframe4?.origin.x = (frame4?.origin.x)!-(lblframe4?.size.width)!-5
        twoRatelbl?.frame = lblframe4!
        twoRatelbl?.text = "(\(UInt(star2_rating)))"
        
        var lblframe5 = oneRatelbl?.frame
        lblframe5?.origin.x = (frame5?.origin.x)!-(lblframe5?.size.width)!-5
        oneRatelbl?.frame = lblframe5!
        oneRatelbl?.text = "(\(UInt(star1_rating)))"
        
        fiveRateView?.backgroundColor = darkgreen
        fiveRateView?.layer.cornerRadius = 3
        
        fourRateView?.backgroundColor = green
        fourRateView?.layer.cornerRadius = 3
        
        threeRateVIew?.backgroundColor = darkyellow
        threeRateVIew?.layer.cornerRadius = 3
        
        twoRateVIew?.backgroundColor = yellow
        twoRateVIew?.layer.cornerRadius = 3
        
        oneRateView?.backgroundColor = red
        oneRateView?.layer.cornerRadius = 3
        
    }
    
    
    @IBAction func star_method(_ sender : UIButton) -> Void {
        StarOne.setImage(UIImage.init(named: star_Full1), for: .normal)
        StarTwo.setImage(UIImage.init(named: star_Full1), for: .normal)
        StarThree.setImage(UIImage.init(named: star_Full1), for: .normal)
        StarFour.setImage(UIImage.init(named: star_Full1), for: .normal)
        StarFive.setImage(UIImage.init(named: star_Full1), for: .normal)
        if sender.tag == 1 {
            StarTwo.setImage(UIImage.init(named: star_empty1), for: .normal)
            StarThree.setImage(UIImage.init(named: star_empty1), for: .normal)
            StarFour.setImage(UIImage.init(named: star_empty1), for: .normal)
            StarFive.setImage(UIImage.init(named: star_empty1), for: .normal)
            rating = 1
        }
        else if sender.tag == 2 {
            StarThree.setImage(UIImage.init(named: star_empty1), for: .normal)
            StarFour.setImage(UIImage.init(named: star_empty1), for: .normal)
            StarFive.setImage(UIImage.init(named: star_empty1), for: .normal)
            rating = 2
        }
        else if sender.tag == 3 {
            StarFour.setImage(UIImage.init(named: star_empty1), for: .normal)
            StarFive.setImage(UIImage.init(named: star_empty1), for: .normal)
            rating = 3
        }
        else if sender.tag == 4 {
            StarFive.setImage(UIImage.init(named: star_empty1), for: .normal)
            rating = 4
        }
        else if sender.tag == 5 {
            rating = 5
        }
        writeReviewbtn.isHighlighted = false
        writeReviewbtn.isUserInteractionEnabled = true
    }
    
    
    @IBAction func WriteReviewButtonclick() -> Void{
        let userSession = UserSession().getUserSession()
        if userSession.userId == nil {
            UserDefaults.standard.set(true, forKey: "loginFrom_another")
            let user = self.storyboard?.instantiateViewController(withIdentifier: "user") as! UINavigationController
            self.present(user, animated: true, completion: nil)
            
        }
        else{
            if Is_store {
                self.Rate_store()
            }
            else{
                self.Rate_Product()
            }
        }
    }
    
    func Rate_store() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            let usersession = UserSession().getUserSession()
            let parameters = ["apikey":API_KEY,
                              "user_id":usersession.userId! as String,
                              "store_id":(store?.store_id!)! as String,
                              "totalstar":"\(rating)",
            ]
            
            
            Alamofire.request(ADD_STORE_RATING, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status")as! Bool == true) {
                        let alert = DXAlertView(title: nil, contentText: "التقييم بنجاح", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                            UserDefaults.standard.set(true, forKey: ratesuccess)
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        let alert = DXAlertView(title: nil, contentText: "كنت تقييمه بالفعل", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                    }
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    
                }
                
            })
        }
        else{
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
            
        }
    }
    
    
    func Rate_Product() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            let usersession = UserSession().getUserSession()
            let parameters = ["apikey":API_KEY,
                              "user_id":usersession.userId! as String,
                              "product_id":(product?.product_id!)! as String,
                              "totalstar":"\(rating)",
                              "taste":"4",
                              "mixability":"4",
                              "efficacy":"4",
                              "valueformoney":"4",
                
            ]
            
            
            Alamofire.request(ADD_PRODUCT_RATING, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status")as! Bool == true) {
                        
                        let alert = DXAlertView(title: nil, contentText: "التقييم بنجاح", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                            UserDefaults.standard.set(true, forKey: ratesuccess)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else{
                        let alert = DXAlertView(title: nil, contentText: "كنت تقييمه بالفعل ", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    
                }
                
            })
        }
        else{
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
    }

    
    @IBAction func BackButton() -> Void {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
