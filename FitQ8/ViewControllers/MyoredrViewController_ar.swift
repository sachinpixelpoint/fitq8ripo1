//
//  MyoredrViewController_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 19/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class MyoredrViewController_ar: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet var OrderTable: UITableView!
    @IBOutlet var norecordLbl : UILabel!
    var orderArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OrderTable.delegate = self
        OrderTable.dataSource = self
        self.navigationItem.title = "طلبي"
        norecordLbl.text = No_record_ar
        self.automaticallyAdjustsScrollViewInsets = false
        OrderTable.register(UINib(nibName: "Order_ar", bundle: nil), forCellReuseIdentifier: "cell")
        norecordLbl.isHidden = true
        self.Get_myOrders()
        
    }
    
    
    func Get_myOrders() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            let user = UserSession().getUserSession()
            
            let parameters = ["apikey":API_KEY,
                              "client_id": user.userId! as String]
            
            Alamofire.request(Get_Orders_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        self.orderArray = dicEntry.mutableCopy() as! NSMutableArray
                        let ar = self.orderArray.reverseObjectEnumerator().allObjects as NSArray
                        self.orderArray = ar.mutableCopy() as! NSMutableArray
                        self.OrderTable.reloadData()
                        print(dicEntry)
                    }
                    else{
                        //// print nil data
                    }
                    if self.orderArray.count == 0 {
                        self.norecordLbl.isHidden = false
                    }
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed_ar, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.Get_myOrders()
                    }
                }
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,
            leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! CustumCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        OrderTable.separatorStyle = UITableViewCellSeparatorStyle.none
        let orderDic = orderArray.object(at: indexPath.row) as! NSDictionary
        
        let formate = DateFormatter()
        formate.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dstr: String? = (orderDic.object(forKey: "order_placed") as? String)
        let date: Date? = formate.date(from: dstr!)
        formate.dateFormat = "dd MMMM yyyy HH:mm"
        let datestr: String = formate.string(from: date!)
        cell.order_dateLbl?.text = datestr
        cell.order_idLbl?.text = "رقم الطلب : \(orderDic.object(forKey: "order_id") as! String)"
        //        cell.order_statusLbl?.text = orderDic.object(forKey: "orderstatus") as? String
        cell.order_statusLbl?.isHidden = true
        
        //        let itemar = (orderDic.object(forKey: "productarray") as! String).objectFromJSONString() as! NSArray
        let dic = (orderDic.object(forKey: "productarray") as! String).objectFromJSONString() as! NSDictionary
        
        cell.storeName?.text = dic.object(forKey: "product_name_ar") as! String?
        cell.storeImage?.setimage(link: dic.object(forKey: "product_image") as! String?)
        
        cell.cancl_reorderBtn?.addTarget(self, action: #selector(self.CancelOrder(_:)), for: .touchUpInside)
         cell.cancl_reorderBtn?.isHidden = false
        cell.cancl_reorderImg?.isHidden = false
        cell.cancl_reorderLbl?.textColor = UIColor.black
        
        if orderDic.object(forKey: "orderstatus") as! String == "3" {
            cell.cancl_reorderImg?.isHidden = true
            cell.cancl_reorderLbl?.text = "طلب ألغيت"
            cell.cancl_reorderBtn?.isHidden = true
            cell.cancl_reorderLbl?.textColor = UIColor.red
            
            var frame = cell.cancl_reorderLbl?.frame
            frame?.origin.x = (cell.cancl_reorderView?.frame.size.width)!/2-(cell.cancl_reorderLbl?.frame.size.width)!/2
            frame?.origin.y = (cell.cancl_reorderView?.frame.size.height)!/2-(cell.cancl_reorderLbl?.frame.size.height)!/2
            cell.cancl_reorderLbl?.frame = frame!
        }
//        else if orderDic.object(forKey: "orderstatus") as! String == "delivered" {
//            cell.cancl_reorderImg?.image = UIImage.init(named: "reorder_blue")
//            cell.cancl_reorderLbl?.text = "REORDER"
//            cell.cancl_reorderBtn?.addTarget(self, action: #selector(self.ReorderMethod(_ :)), for: .touchUpInside)
//        }
        else{
            cell.cancl_reorderImg?.image = UIImage.init(named: "cancel1")
            cell.cancl_reorderLbl?.text = "إلغاء طلب"
            
            var frame = cell.cancl_reorderLbl?.frame
            let imgW = (cell.cancl_reorderImg?.frame.size.width)!/2
            frame?.origin.x = (cell.cancl_reorderView?.frame.size.width)!/2-(cell.cancl_reorderLbl?.frame.size.width)!/2-imgW
            frame?.origin.y = (cell.cancl_reorderView?.frame.size.height)!/2-(cell.cancl_reorderLbl?.frame.size.height)!/2
            cell.cancl_reorderLbl?.frame = frame!
            
            var imgframe = cell.cancl_reorderImg?.frame
            imgframe?.origin.x = (cell.cancl_reorderLbl?.frame.origin.x)!+(cell.cancl_reorderLbl?.frame.size.width)!+10
            cell.cancl_reorderImg?.frame = imgframe!
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderdic = orderArray.object(at: indexPath.row) as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "orderdetailvc") as! OrderDetailVC_ar
        vc.orderDic = orderdic
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func BackButton() -> Void {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /////////////////////  Cancel Order method///////////////
    
    func CancelOrder(_ sender : UIButton) -> Void {
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: OrderTable)
        let indexPath = OrderTable.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        let orderDic = orderArray.object(at: indexPath.row) as! NSDictionary
        
        let alert = DXAlertView(title: nil, contentText: "هل أنت واثق بأك تريد أن تلغي طلب الشراء", leftButtonTitle: "Cancel", rightButtonTitle: "Yes")
        alert?.show()
        alert?.rightBlock = {() -> Void in
            self.Cancel_Order(orderDic.object(forKey: "order_id") as! String)
        }
        
    }
    
    
    func Cancel_Order(_ order_id : String) -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "order_id": order_id as String]
            
            Alamofire.request(CANCEL_ORDER, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        
                        self.Get_myOrders()
                    }
                    else{
                        
                    }
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed_ar, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.Cancel_Order(order_id)
                    }
                }
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,
                                    leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
    }

    
    /////////////////////////// Reorder method /////////////////
//    func ReorderMethod(_ sender : UIButton) -> Void {
//        let center = sender.center
//        let  rootViewPoint = sender.superview?.convert(center, to: OrderTable)
//        let indexPath = OrderTable.indexPathForRow(at: rootViewPoint!)!
//        print("\(indexPath)")
//        
//        let orderDic = orderArray.object(at: indexPath.row) as! NSDictionary
//        let itemArr = (orderDic.object(forKey: "orderarray") as! String).objectFromJSONString() as! NSArray
//        for i in 0..<itemArr.count {
//            let itemdic = itemArr.object(at: i) as! NSDictionary
//            
//            let moc = (UIApplication.shared.delegate
//                as! AppDelegate).persistentContainer.viewContext
//            
//            let cart = NSEntityDescription.insertNewObject(forEntityName: "Cart", into: moc) as! Cart
//            cart.price = itemdic.object(forKey: "product_price") as? String
//            cart.product_id = itemdic.object(forKey: "product_id") as? String
//            cart.product_name = itemdic.object(forKey: "product_name") as? String
//            cart.product_name_ar = itemdic.object(forKey: "product_name_ar") as? String
//            cart.product_des = itemdic.object(forKey: "product_description") as? String
//            cart.product_image = itemdic.object(forKey: "") as? String
//            cart.store_id = itemdic.object(forKey: "store_id") as? String
//            cart.quentity = itemdic.object(forKey: "product_quantity") as? String
//            print(cart)
//            
//            do {
//                try moc.save()
//            } catch {
//                print(error)
//            }
//            
//            let alert = DXAlertView(title: nil, contentText: "تم إضافة العنصر إلى السلة",
//                leftButtonTitle: nil, rightButtonTitle: "OK")
//            alert?.show()
//        }
//        
//    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
