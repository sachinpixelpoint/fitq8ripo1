//
//  AreaVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 02/02/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class AreaVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var AreasearchBar : UISearchBar!
    @IBOutlet var AreaTable : UITableView!
    var AreaArr = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Utility().setStatusBarBackgroundColor(color: UIColor(red: 30/255.0, green: 45/255.0, blue: 54/255.0, alpha: 1.0))
        self.navigationItem.title = "Select Area"
        
        self.automaticallyAdjustsScrollViewInsets = false
        AreaArr = CoredataClass().getAreaData().mutableCopy() as! NSMutableArray
        AreaTable.frame = CGRect(x: 0, y: 110, width: self.view.frame.size.width, height: self.view.frame.size.height-110)
        AreaTable.delegate = self
        AreaTable.dataSource = self
        
        let shadowPath = UIBezierPath(rect: AreasearchBar.bounds)
        AreasearchBar.layer.masksToBounds = false
        AreasearchBar.layer.shadowColor = UIColor.lightGray.cgColor
        AreasearchBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        AreasearchBar.layer.shadowOpacity = 0.7
        AreasearchBar.layer.shadowPath = shadowPath.cgPath
        
        AreasearchBar.barTintColor = UIColor.white
        AreasearchBar.tintColor = UIColor.black
        AreasearchBar.delegate = self
        AreasearchBar.layer.borderWidth = 1;
        AreasearchBar.layer.borderColor = UIColor.white.cgColor

        // Do any additional setup after loading the view.
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AreaArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = AreaTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let area = AreaArr.object(at: indexPath.row) as! Area
        cell.textLabel?.text = area.area_name! as String
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16.0)
        cell.textLabel?.textColor = UIColor.black
        let areaid = UserDefaults.standard.object(forKey: area_id) as? String
        if areaid != nil && areaid == area.area_id{
            cell.textLabel?.textColor = UIColor.blue
        }
        return cell
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let area = AreaArr.object(at: indexPath.row) as! Area
        let areaId = UserDefaults.standard.object(forKey: area_id) as? String
        if areaId == nil {
            UserDefaults.standard.set(area.area_id, forKey: area_id)
            self.dismiss(animated: true, completion: nil)
        }
        else{
            let previousArea = CoredataClass().getAreaFromAreaId(areaId! as String) as Area
            if previousArea.area_id == area.area_id {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                UserDefaults.standard.set(area.area_id, forKey: area_id)
                UserDefaults.standard.set(true, forKey: "areaChanged")
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("Begin")
        
        AreasearchBar?.setShowsCancelButton(false, animated: true)
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("End")

            AreasearchBar?.endEditing(true)
            AreasearchBar?.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("cancel")
        searchBar.endEditing(true)
        AreasearchBar?.text = nil
        self.searchBarTextDidEndEditing(searchBar)
        AreaTable.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Change")

            AreaArr.removeAllObjects()
            if searchText.characters.count == 0 {
                AreaArr = CoredataClass().getAreaData().mutableCopy() as! NSMutableArray
            }
            else{
                AreaArr = CoredataClass().getAreaSearchData(searchText).mutableCopy() as! NSMutableArray
            }
            AreaTable.reloadData()
            print(AreaArr)
    }
    
    @IBAction func BackButton() -> Void {
        
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
