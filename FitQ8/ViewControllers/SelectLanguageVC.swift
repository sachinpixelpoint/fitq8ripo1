//
//  SelectLanguageVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 16/02/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class SelectLanguageVC: UIViewController {
    
    @IBOutlet var EnglishBtn : UIButton!
    @IBOutlet var ArabicBtn : UIButton!
    let bluecolor = UIColor.init(red: 47/255, green: 37/255, blue: 191/255, alpha: 1)
    
    
    //    var frame : CGRect!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        EnglishBtn.layer.cornerRadius = EnglishBtn.frame.size.height/2
        EnglishBtn.layer.borderWidth = 2;
        EnglishBtn.layer.borderColor = UIColor.black.cgColor
        ArabicBtn.layer.cornerRadius =  ArabicBtn.frame.size.height/2
        ArabicBtn.layer.borderWidth = 2;
        ArabicBtn.layer.borderColor = UIColor.black.cgColor
        
        self.tabBarController?.tabBar.isHidden = true
        Utility().setStatusBarBackgroundColor(color: UIColor(red: 30/255.0, green: 45/255.0, blue: 54/255.0, alpha: 1.0))
        
        
        EnglishBtn.addTarget(self, action: #selector(self.EnglishMethod(_:)), for: .touchUpInside)
        ArabicBtn.addTarget(self, action: #selector(self.ArabicMethod(_:)), for: .touchUpInside)
        
    }
    
    
    
    func EnglishMethod(_ sender : UIButton) {
        UserDefaults.standard.set(english, forKey: lang)
        let mainStoryBoard : UIStoryboard!
        mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "selectarea") as! AreaSelectionVC
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func ArabicMethod(_ sender : UIButton) {
        UserDefaults.standard.set(arabic, forKey: lang)
        let mainStoryBoard : UIStoryboard!
        mainStoryBoard = UIStoryboard(name: "Main_ar", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "selectarea") as! AreaSelectionVC_ar
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
