//
//  StoreDataVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 23/02/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class StoreDataVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    var store : Store!
    @IBOutlet var categoryTabel : FZAccordionTableView!
    var catArray = NSMutableArray()
    var catItemArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = store.store_name
        categoryTabel.allowMultipleSectionsOpen = true
        categoryTabel.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCellReuseIdentifier")
        categoryTabel.register(UINib.init(nibName: "storedataCell", bundle: nil), forCellReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        
        self.GetStoreData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
        if UserDefaults.standard.bool(forKey: ratesuccess){
            UserDefaults.standard.set(false, forKey: ratesuccess)
            self.GetStoreData()
        }
    }
    
    func GetStoreData() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "storeid":store.store_id! as String]
            print(parameters)
            Alamofire.request(STORE_DATA, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        self.catArray = NSMutableArray(array: jsonObject.object(forKey: "data") as! NSArray) as NSMutableArray
                        self.categoryTabel?.delegate = self
                        self.categoryTabel?.dataSource = self
                        self.categoryTabel?.reloadData()
                    }
                    else{
                        let alert = DXAlertView(title: nil, contentText: No_record, leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetStoreData()
                    }
                    
                }
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    
    
    ///////////////// Table view methods ///////////////
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let productdic = catArray.object(at: section) as! NSDictionary
        let productAr = (productdic.object(forKey: "product") as! String).objectFromJSONString() as! NSArray
        return productAr.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return catArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView(tableView, heightForHeaderInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CustumCell = categoryTabel.dequeueReusableCell(withIdentifier: "cell") as! CustumCell!
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let productdic = catArray.object(at: indexPath.section) as! NSDictionary
        let productAr = (productdic.object(forKey: "product") as! String).objectFromJSONString() as! NSArray
        let product = productAr.object(at: indexPath.row) as! NSDictionary

        cell.storeName?.text = product.object(forKey: "product_name") as? String
        cell.storeName?.numberOfLines = 0
        cell.store_des?.text = product.object(forKey: "product_description") as? String
        cell.store_des?.numberOfLines = 0
        cell.storeImage?.setimage(link: product.object(forKey: "product_image") as! String?)
        
        cell.lineLbl?.isHidden = false
        if productAr.count <= 1 {
            cell.lineLbl?.isHidden = true
        }
        
        cell.Rateview?.notSelectedImage = UIImage.init(named: star_empty)
        cell.Rateview?.halfSelectedImage = UIImage.init(named: star_Half)
        cell.Rateview?.fullSelectedImage = UIImage.init(named: star_Full)
        cell.Rateview?.editable = false;
        cell.Rateview?.maxRating = 5;
        cell.Rateview?.midMargin = 0;
        cell.Rateview?.isUserInteractionEnabled = false;
        var rating : Float = 0.0
        if (product.object(forKey: "rating") as? String)?.isEmpty == false {
            let datadic = (product.object(forKey: "rating") as! String).objectFromJSONString() as! NSDictionary
            let totalRatingCount = (datadic.object(forKey: "totalstar") as! NSString).floatValue
            let baseRating = (datadic.object(forKey: "totalrecord") as! NSString).floatValue
            rating = totalRatingCount/baseRating
        }
        cell.Rateview?.rating = rating
        cell.RateButton?.addTarget(self, action: #selector(self.didClickRateButton), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = FZAccordionTableViewHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        
        let colorVIew = UIView(frame: CGRect(x: 0, y: 1, width: tableView.frame.size.width, height: 48))
        colorVIew.backgroundColor = UIColor.white //UIColor.init(red: 0/255, green: 0/255, blue: 205/255, alpha: 1)
        /* Create custom view to display section header... */
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.frame.size.width-15, height: 50))
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(18))
        
        let productdic = catArray.object(at: section) as! NSDictionary
        let productAr = (productdic.object(forKey: "product") as! String).objectFromJSONString() as! NSArray
        
        label.text = productdic.object(forKey: "category_name") as? String
        label.textAlignment = .left
        label.textColor = UIColor.black
        colorVIew.addSubview(label)
        label.sizeToFit()
        var lblframe = label.frame
        lblframe.origin.y = 25-lblframe.size.height/2
        label.frame = lblframe
        
        let itemNobl = UILabel(frame: CGRect(x: label.frame.origin.x+label.frame.size.width+10, y: 10, width: 30, height: 30))
        itemNobl.backgroundColor = UIColor.init(red: 189/255, green: 16/255, blue: 0/255, alpha: 1)
        itemNobl.textColor = UIColor.white
        itemNobl.font = UIFont.systemFont(ofSize: 14)
        itemNobl.layer.masksToBounds = true
        itemNobl.layer.borderColor = UIColor.white.cgColor
        itemNobl.layer.borderWidth=2
        itemNobl.text = "\(UInt(productAr.count))"
        itemNobl.textAlignment = NSTextAlignment.center
        itemNobl.sizeToFit()
        var frame = itemNobl.frame
        frame.size.width = frame.size.width+20
        //        frame.size.height = frame.size.height+5
        frame.origin.y = 25-frame.size.height/2
        itemNobl.frame = frame
        itemNobl.layer.cornerRadius = itemNobl.frame.size.height/2
        colorVIew.addSubview(itemNobl)
        
        
        if lblframe.size.width > colorVIew.frame.size.width-(30+itemNobl.frame.size.width) {
            var lblframe1 = label.frame
            lblframe1.size.width = colorVIew.frame.size.width-(30+itemNobl.frame.size.width)
            label.frame = lblframe1
            itemNobl.frame =  CGRect(x: label.frame.origin.x+label.frame.size.width+10, y: itemNobl.frame.origin.y, width: itemNobl.frame.size.width, height: itemNobl.frame.size.height)
        }
        
        
        view.addSubview(colorVIew)
        return view
    }
    
    // FZAccordionTableViewDelegate////// -
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }
    
    ///////////////////// Did selecct method //////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let productdic = catArray.object(at: indexPath.section) as! NSDictionary
        let productAr = (productdic.object(forKey: "product") as! String).objectFromJSONString() as! NSArray
        let productdata = productAr.object(at: indexPath.row) as! NSDictionary
        
        let productDetail = self.storyboard?.instantiateViewController(withIdentifier: "productdetail") as! ProductDetailVC
        productDetail.product = self.createProduct(productdata)
        self.navigationController?.pushViewController(productDetail, animated: true)

    }
    
    
    
    /////////////  Did click button Rate ////////
    func didClickRateButton(_ sender : UIButton) -> Void  {
       
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: categoryTabel)
        let indexPath = categoryTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        
        let productdic = catArray.object(at: indexPath.section) as! NSDictionary
        let productAr = (productdic.object(forKey: "product") as! String).objectFromJSONString() as! NSArray
        let productdata = productAr.object(at: indexPath.row) as! NSDictionary
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "rateNav") as! UINavigationController
        Variables.product = self.createProduct(productdata)
        Variables.Is_Store = false
        self.present(vc, animated: true, completion: nil)

    }
    
    
    func createProduct(_ productdata : NSDictionary) -> Product {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        let product = NSEntityDescription.insertNewObject(forEntityName: "Product", into: moc) as! Product
        product.price = productdata.object(forKey: "price") as? String
        product.product_id = productdata.object(forKey: "product_id") as? String
        product.product_name = productdata.object(forKey: "product_name") as? String
        product.product_name_ar = productdata.object(forKey: "product_name_ar") as? String
        product.product_description = productdata.object(forKey: "product_description") as? String
        product.product_description_ar = productdata.object(forKey: "product_description_ar") as? String
        product.product_image = productdata.object(forKey: "product_image") as? String
        product.store_id = productdata.object(forKey: "store_id") as? String
        product.product_quantity = productdata.object(forKey: "product_quantity") as? String
        product.buying_guide = productdata.object(forKey: "buying_guide") as? String
        product.supplement_info = productdata.object(forKey: "supplement_info") as? String
        product.user_guide = productdata.object(forKey: "user_guide") as? String
        product.rating = productdata.object(forKey: "rating") as? String
        product.store_name = productdata.object(forKey: "store_name") as? String
        
        return product
    }
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
