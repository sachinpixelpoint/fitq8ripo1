  //
//  HomeViewController_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 24/12/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class HomeViewController_ar: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate {
    
    @IBOutlet var StoreTabel: UITableView!
    @IBOutlet var norecordLbl : UILabel!
    var searchTable : UITableView!
     var  storeArray = NSMutableArray()
    var filterarray = NSMutableArray()
    @IBOutlet var SearchBar : UISearchBar!
    var searchBarEdition = false
    
        override func viewDidLoad() {
        super.viewDidLoad()
        norecordLbl.text = No_record_ar
        norecordLbl.isHidden = true
    
        self.automaticallyAdjustsScrollViewInsets = false
        SearchBar.delegate = self
        
        StoreTabel.delegate=self
        StoreTabel.dataSource=self
        StoreTabel.register(UINib(nibName: "Homecell_ar", bundle: nil), forCellReuseIdentifier: "cell")

        searchTable = UITableView()
        searchTable.register(UINib(nibName: "searchCell", bundle: nil), forCellReuseIdentifier: "searchCell")
        
        self.GetStores()

    }

    
    func GetStores() -> Void {
        if  Utility().hasInternetConnection() {
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
             let area = CoredataClass().getAreaFromAreaId(UserDefaults.standard.object(forKey: area_id) as! String) as Area
            
            let parameters = ["apikey":API_KEY,
                              "areaid":area.area_id! as String]
            Alamofire.request(STORE_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    CoredataClass().DeleteAllData("Store")
                     if (jsonObject.object(forKey: "status") as! Bool) == true {
                        self.norecordLbl.isHidden = true
                        let dataAr = NSMutableArray()
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        for i in 0..<dicEntry.count  {
                            let dictCat = dicEntry.object(at: i) as! NSDictionary
                            print(dictCat)
                            
                            let className = NSStringFromClass(Store.self) as String
                            let entity = NSEntityDescription.entity(forEntityName:className, in: moc)!
                            let cat = (NSManagedObject(entity: entity, insertInto: moc) as! Store)
                            cat.safeSetValuesForKeys(with: dictCat as Any? as? [AnyHashable: Any] ?? [:] )
                            dataAr.add(cat)
                            do {
                                try moc.save()
                            } catch {
                                print(error)
                            }
                        }
                    }
                    else{
                        self.norecordLbl.isHidden = false
                    }
                    self.storeArray = CoredataClass().getStoreData().mutableCopy() as! NSMutableArray
                    self.StoreTabel.reloadData()
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed_ar, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetStores()
                    }
                    print("Request failed with error: \(error)")
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,
            leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    

    
    ///////////////// Table view methods ///////////////
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBarEdition  {
            return self.filterarray.count
        }
        return self.storeArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        if searchBarEdition {
            
            let cell:CustumCell = searchTable.dequeueReusableCell(withIdentifier: "searchCell") as! CustumCell!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            let store = filterarray.object(at: indexPath.row) as! Store
            cell.textLabel?.text = store.store_name
            
            cell.accessoryType = .disclosureIndicator
            return cell
            
        }
        else{
            let cell:CustumCell = StoreTabel.dequeueReusableCell(withIdentifier: "cell") as! CustumCell!
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            let store = storeArray.object(at: indexPath.row) as! Store
            
            cell.storeName?.text = store.store_name_ar
            cell.storeImage?.setimageWithout_PH(link: store.store_image)
            
            cell.Rateview?.notSelectedImage = UIImage.init(named: star_empty)
            cell.Rateview?.halfSelectedImage = UIImage.init(named: star_Half)
            cell.Rateview?.fullSelectedImage = UIImage.init(named: star_Full)
            cell.Rateview?.editable = false;
            cell.Rateview?.maxRating = 5;
            cell.Rateview?.midMargin = 0;
            cell.Rateview?.isUserInteractionEnabled = true;
            var rating : Float = 0.0
            if store.ratingdata?.isEmpty == false {
                let datadic = (store.ratingdata!).objectFromJSONString() as! NSDictionary
                print(datadic)
                let totalRatingCount = (datadic.object(forKey: "totalstar") as! NSString).floatValue
                let baseRating = (datadic.object(forKey: "totalrecord") as! NSString).floatValue
                rating = totalRatingCount/baseRating
            }
            
            cell.Rateview?.rating = rating
            cell.RateButton?.addTarget(self, action: #selector(self.didClickRateButton), for: .touchUpInside)
            
            return cell
           
        }
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let productVC = self.storyboard?.instantiateViewController(withIdentifier: "storedata") as! StoreDataVC_ar
        if searchBarEdition {
            productVC.store = filterarray.object(at: indexPath.row) as! Store
        }
        else{
            productVC.store = storeArray.object(at: indexPath.row) as! Store
        }
        self.navigationController?.pushViewController(productVC, animated: true)
    }
    
    
    func didClickRateButton(_ sender : UIButton) -> Void  {
        
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: StoreTabel)
        let indexPath = StoreTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "rateNav") as! UINavigationController
        Variables.store = storeArray.object(at: indexPath.row) as? Store
        Variables.Is_Store = true
        self.present(vc, animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true

        if UserDefaults.standard.bool(forKey: "areaChanged") || UserDefaults.standard.bool(forKey: ratesuccess) {
            if UserDefaults.standard.bool(forKey: "areaChanged") {
                CoredataClass().DeleteAllData("Cart")
                CoredataClass().DeleteAllData("MyList")
            }
            UserDefaults.standard.set(false, forKey: ratesuccess)
            UserDefaults.standard.set(false, forKey: "areaChanged")
            self.GetStores()
        }
       
        /////////// Add left panel button /////
        self.setTitle_onNavigation()
        
        Utility().AddSidePanel(VC: self)
        
        UserDefaults.standard.set(self.tabBarController?.selectedIndex, forKey: SelectedTab)
        UserDefaults.standard.set(true, forKey: tabCondition)
        
        
        let cartarray = CoredataClass().getCartData() as NSArray
        
        let cartBtn = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(40), height: CGFloat(40)))
        cartBtn.setImage(UIImage(named: "cart_icon")!, for: .normal)
        cartBtn.addTarget(self, action: #selector(self.CartbuttonItem), for: .touchUpInside)
        if cartarray.count > 0 {
            let cartitelbl = UILabel(frame: CGRect(x: CGFloat(30), y: CGFloat(2), width: CGFloat(15), height: CGFloat(8)))
            cartitelbl.font = UIFont.systemFont(ofSize: 8)
            cartitelbl.backgroundColor = UIColor.white
            cartitelbl.clipsToBounds = true
            cartitelbl.layer.cornerRadius = cartitelbl.frame.size.height / 2
            cartitelbl.textAlignment = .center
            cartitelbl.textColor = UIColor.blue
            cartitelbl.text = "\(UInt(cartarray.count))"
            cartBtn.addSubview(cartitelbl)
        }
        let cartbtn = UIBarButtonItem(customView: cartBtn)
        
        let btn2 = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(40), height: CGFloat(40)))
        btn2.setImage(UIImage(named: "search"), for: .normal)
        btn2.addTarget(self, action: #selector(self.searchmethod), for: .touchUpInside)
        let item2 = UIBarButtonItem()
        item2.customView = btn2
        
        self.navigationItem.rightBarButtonItems = [item2,cartbtn]
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchDisplayController?.setActive(false, animated: false)
        UserDefaults.standard.set(false, forKey: tabCondition)
    }
    
    
    func searchmethod() -> Void {
        SearchBar.becomeFirstResponder()
        if searchBarEdition == false {
            searchBarEdition = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
    }
    
    
    func setTitle_onNavigation() {
        var areaName: String
        let area = CoredataClass().getAreaFromAreaId(UserDefaults.standard.object(forKey: area_id) as! String) as Area
        areaName = area.area_name_ar!

        var mystr: String
        if (areaName.characters.count ) > 17 {
            mystr = ((areaName as NSString).substring(to: 17))
            mystr = "\(mystr)...  ⌵"
        }
        else {
            mystr = "\(areaName)  ⌵"
        }
        let navBtn = UIButton(frame: CGRect.zero)
        navBtn.setTitle(mystr, for: .normal)
        navBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        navBtn.setTitleColor(UIColor.white, for: .normal)
        navBtn.addTarget(self, action: #selector(self.Areafield_Method), for: .touchUpInside)
        navBtn.sizeToFit()
        self.navigationItem.titleView = navBtn
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("Begin")
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("End")
        searchBarEdition = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("cancel")
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Change")
        filterarray.removeAllObjects()
        filterarray = CoredataClass().getStoreSearchData(searchText).mutableCopy() as! NSMutableArray
        print(filterarray)
    }
    
    
    func CartbuttonItem() -> Void{
        
        self.tabBarController?.selectedIndex = 2
    }
    
    
    
    /////////////////////// //// Drop Down Button for Area///////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    
    func Areafield_Method()-> Void {
        let areavc = self.storyboard?.instantiateViewController(withIdentifier: "areavc") as! UINavigationController
        self.present(areavc, animated: true, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
    }
}
