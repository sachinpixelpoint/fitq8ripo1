//
//  CheckCodeVC_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 28/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class CheckCodeVC_ar: UIViewController {
    
    @IBOutlet var CodetextFeild : UITextField!
    @IBOutlet var submitbtn : UIButton!
    var email_id : String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "OTP"
    }

    func Check_code() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "user_email":email_id as String,
                              "password_salt":CodetextFeild.text! as String
                
            ]
            Alamofire.request(CHECK_CODE, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status")as! Bool == true) {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "reset") as! ResetPasswordVC_ar
                        vc.code = self.CodetextFeild.text! as String
                        vc.email_id = self.email_id as String
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else{
                        
                        let alert = DXAlertView(title: nil, contentText: "الرمز غير صحيح", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                        }
                    }
                    
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    
                }
                
            })
        }
        else{
            
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func submitbtn_method() {
        
        self.Check_code()
        
    }
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
