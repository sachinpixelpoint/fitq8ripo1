//
//  UserViewController_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 18/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class UserViewController_ar: UIViewController {

    @IBOutlet var Label1:UILabel?
    @IBOutlet var Label2:UILabel?
    @IBOutlet var Label3:UILabel?
    @IBOutlet var Label4:UILabel?
    @IBOutlet var Image1:UIImageView?
    @IBOutlet var Image2:UIImageView?
    @IBOutlet var Image3:UIImageView?
    @IBOutlet var Image4:UIImageView?
    @IBOutlet var Image5:UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Image5?.layer.borderColor = UIColor.white.cgColor
        Image5?.layer.cornerRadius = (Image5?.frame.size.height)!/2
        Image5?.clipsToBounds = true
        Image5?.layer.masksToBounds = true
        self.navigationItem.title = "ملف المستخدم "
    
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(self.tabBarController?.selectedIndex, forKey: SelectedTab)
        UserDefaults.standard.set(true, forKey: tabCondition)
        
        let Usersession = UserSession().getUserSession()
        
        if Usersession.userId == nil {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController_ar
            self.navigationController?.pushViewController(controller, animated: false)
        }
        else{
            self.setData()
        }
        
        if !UserDefaults.standard.bool(forKey: "loginFrom_another") {
            /////////// Add left panel button /////
            Utility().AddSidePanel(VC: self)
        }
        
    }
    
    @IBAction func changepass_method(){
        let ResetVC = self.storyboard?.instantiateViewController(withIdentifier: "reset") as! ResetPasswordVC_ar
        ResetVC.is_pssUpdate = true
        self.navigationController?.pushViewController(ResetVC, animated: true)
        
    }
    
    
    func setData() -> Void {
        let Usersession = UserSession().getUserSession()
        
        Label1?.text = Usersession.userName! as String
        Label2?.text = Usersession.email! as String
        Label3?.text = Usersession.phoneNo! as String
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UserDefaults.standard.set(false, forKey: tabCondition)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
