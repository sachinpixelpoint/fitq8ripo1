//
//  TabBarVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 12/20/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit
var selectionIndicatorImage: UIImage?

class TabBarVC: UITabBarController,UITabBarControllerDelegate {

   override func viewDidLoad() {
        super.viewDidLoad()

    self.delegate = self
    
    Utility().setStatusBarBackgroundColor(color: UIColor(red: 30/255.0, green: 45/255.0, blue: 54/255.0, alpha: 1.0))
    
    let numberOfItems = CGFloat(tabBar.items!.count)
    let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)

    /////////// set selectionIndicatorColor
    tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(red: 16/255.0, green: 0/255.0, blue: 182/255.0, alpha: 1.0), size: tabBarItemSize)
    
    tabBar.frame.size.width = self.view.frame.width + 4
    tabBar.frame.origin.x = -2
    
    self.selectedIndex = UserDefaults.standard.integer(forKey: SelectedTab)

      }
    
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController == tabBarController.viewControllers?[1] {
            if self.selectedIndex == 1{
                return false
            }
            else{
                return true
            }
        }
        else {
            return true
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        UserDefaults.standard.set(tabBarController.selectedIndex, forKey: SelectedTab)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
  
     }
}


