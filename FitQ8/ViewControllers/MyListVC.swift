//
//  MyListVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 30/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class MyListVC: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet var ListTabel : UITableView!
    @IBOutlet var NorecordLbl : UILabel!
    var listArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ListTabel.delegate = self
        ListTabel.dataSource = self
        ListTabel.register(UINib(nibName: "listCell", bundle: nil), forCellReuseIdentifier: "cell")

        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.title = "My list"
        listArray = CoredataClass().GetMyList().mutableCopy() as! NSMutableArray
        NorecordLbl.isHidden = true
        if listArray.count <= 0 {
            NorecordLbl.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ListTabel.dequeueReusableCell(withIdentifier: "cell") as! CustumCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let listproduct = listArray.object(at: indexPath.row) as! Product
        cell.storeImage?.setimage(link: listproduct.product_image)
        cell.storeName?.text = listproduct.product_name
        cell.LikeButton?.addTarget(self, action: #selector(self.unlikeProduct), for: .touchUpInside)
        
        var productPrice : Float = 0.0
        if listproduct.price != nil {
            let sizeArr = (listproduct.price)?.objectFromJSONString() as! NSArray
            if sizeArr.count>0 {
                let sizedic = sizeArr.object(at: 0) as! NSDictionary
                productPrice = NSString(string: sizedic.object(forKey: "price") as! String).floatValue
            }
        }

        cell.priceLbl?.text = "KD \(productPrice)"
        cell.LikeButton?.addTarget(self, action: #selector(self.unlikeProduct(_:)), for: .touchUpInside)
//        cell.lineLbl?.isHidden = false
//        if indexPath.row == listArray.count-1 {
//            cell.lineLbl?.isHidden = true
//        }
        
        cell.Rateview?.notSelectedImage = UIImage.init(named: star_empty)
        cell.Rateview?.halfSelectedImage = UIImage.init(named: star_Half)
        cell.Rateview?.fullSelectedImage = UIImage.init(named: star_Full)
        cell.Rateview?.editable = false;
        cell.Rateview?.maxRating = 5;
        cell.Rateview?.midMargin = 0;
        cell.Rateview?.isUserInteractionEnabled = false;
        var rating : Float = 0.0
        if listproduct.rating?.isEmpty == false {
            let datadic = (listproduct.rating!).objectFromJSONString() as! NSDictionary
            let totalRatingCount = (datadic.object(forKey: "totalstar") as! NSString).floatValue
            let baseRating = (datadic.object(forKey: "totalrecord") as! NSString).floatValue
            rating = totalRatingCount/baseRating
        }
        cell.Rateview?.rating = rating
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let product = listArray.object(at: indexPath.row) as! Product
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "productdetail") as! ProductDetailVC
        vc.product = product
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    
    func unlikeProduct(_ sender : UIButton) -> Void {
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: ListTabel)
        let indexPath = ListTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            let list_item = self.listArray.object(at: indexPath.row) as! Product
            moc.delete(list_item)
            do {
                try moc.save()
            } catch {
                print(error)
            }
            self.listArray.removeObject(at: indexPath.row)
            self.ListTabel.reloadData()
            
            if self.listArray.count <= 0 {
                self.NorecordLbl.isHidden = false
            }
            },completion: { (finished: Bool) -> Void in
                
        })
        
    }
    
    
    
    @IBAction func BackButton() -> Void {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
