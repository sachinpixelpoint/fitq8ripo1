//
//  MyaddressViewController.swift
//  FitQ8
//
//  Created by Manish Kumar on 1/7/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class MyaddressViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var AddressTabel : UITableView!
    @IBOutlet var norecordLbl : UILabel!
    var AddressArray = NSMutableArray()
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.title = "My Address"
        AddressTabel.register(UINib(nibName: "Myaddress", bundle: nil), forCellReuseIdentifier: "cell")
        norecordLbl.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.GetAdd_Method()
    }
    
    
    func GetAdd_Method() -> Void {
        if  Utility().hasInternetConnection() {
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            let user = UserSession().getUserSession()
            
            let parameters = ["apikey":API_KEY,
                "user_id": user.userId! as String]
            
            Alamofire.request(GET_ADD_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        CoredataClass().DeleteAllData("Address")
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        for i in 0..<dicEntry.count  {
                            let dictCat = dicEntry.object(at: i) as! NSDictionary
                            print(dictCat)
                            
                            let className = NSStringFromClass(Address.self) as String
                            let entity = NSEntityDescription.entity(forEntityName:className, in: moc)!
                            let cat = (NSManagedObject(entity: entity, insertInto: moc) as! Address)
                            cat.safeSetValuesForKeys(with: dictCat as Any? as? [AnyHashable: Any] ?? [:] )

                            do {
                                try moc.save()
                            } catch {
                                print(error)
                            }
                        }
                        self.AddressArray = CoredataClass().getAddressData().mutableCopy() as! NSMutableArray
                        print(self.AddressArray)
                        self.AddressTabel.delegate = self
                        self.AddressTabel.dataSource = self
                        self.AddressTabel.reloadData()

                    }
                    else{
                        //// print nil data
                    }
                    if self.AddressArray.count == 0 {
                        self.norecordLbl.isHidden = false
                    }
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetAdd_Method()
                    }
                   
                    
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AddressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! CustumCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        let add = AddressArray.object(at: indexPath.row) as! Address
        let area = CoredataClass().getAreaFromAreaId(add.area_id!) as Area
        
        cell.addresnamelbl?.text = "\(add.address_name!)"
        cell.arealbl?.text = "\(area.area_name!)"
        cell.blocklbl?.text = "Block : \(add.block!)"
        cell.streetlbl?.text = "Street : \(add.street_num!)"
        cell.houselbl?.text = "House No.: \(add.house_num!)"
        cell.phoneNolbl?.text = "Phone No.: \(add.phone!)"
        
        cell.editbtn?.addTarget(self, action: #selector(self.EditkButton_method), for: .touchUpInside)
        cell.cancelbtn?.addTarget(self, action:#selector(self.DeleteButton_method), for: .touchUpInside)
        return cell
    }
    /////Table View Methods/////
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let cell = tableView.cellForRow(at: indexPath as IndexPath)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that cansearchbtnmethod ()be recreated.
    }
    
    
    
    func EditkButton_method(_ sender: UIButton) -> Void{
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: AddressTabel)
        let indexPath = AddressTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        
        let address = AddressArray.object(at: indexPath.row) as! Address
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "addaddress") as! AddAddressVC
        vc.address = address
        vc.Is_editAdd = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func DeleteButton_method(_sender: UIButton) -> Void {
        let center = _sender.center
        let  rootViewPoint = _sender.superview?.convert(center, to: AddressTabel)
        let indexPath = AddressTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")

        let address = AddressArray.object(at: indexPath.row) as! Address
        self.Delete_Address(address)
        if AddressArray.count == 0 {
            norecordLbl.isHidden = false
        }

    }
    
    
    func Delete_Address(_ address : Address) -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false

            let parameters = ["apikey":API_KEY,
                              "address_id": address.address_id! as String]
            Alamofire.request(DELETE_ADD_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        
                        self.AddressArray.remove(address)
                        self.AddressTabel.reloadData()
                        
                    }
                    else{
                        //// print nil data
                    }
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.Delete_Address(address)
                    }
 
                }
                
            })
        }
        else{
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
            
        }
        
    }

    
    @IBAction func AddAddress() -> Void{
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "addaddress") as! AddAddressVC
        vc.Is_editAdd = true
        vc.Is_add = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func BackButton() -> Void {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
