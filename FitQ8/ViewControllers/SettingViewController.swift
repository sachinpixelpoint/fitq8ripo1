//
//  SettingViewController.swift
//  FitQ8
//
//  Created by Manish Kumar on 1/17/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    
    @IBOutlet var Engbtn : UIButton?
    @IBOutlet var Arabicbtn : UIButton?
    @IBOutlet var EngImage : UIImageView!
    @IBOutlet var ArabImage : UIImageView!
    @IBOutlet var Savebtn :UIButton!
    @IBOutlet var notificationSwitch : UISwitch!
    var selectedLang : String!
    var notification : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /////////// Add left panel button /////
        Utility().AddSidePanel(VC: self)

        UserDefaults.standard.set(self.tabBarController?.selectedIndex, forKey: SelectedTab)
        UserDefaults.standard.set(true, forKey: tabCondition)
        
        self.Savebtn.layer.cornerRadius = 10
        selectedLang = UserDefaults.standard.object(forKey: lang) as! String
        notification = UserDefaults.standard.bool(forKey: Notification_on) as Bool
        if notification == true {
            notificationSwitch.setOn(true, animated: false)
        }
        else{
            notificationSwitch.setOn(false, animated: false)
        }
        
        if selectedLang == english {
            EngImage.image = UIImage(named: "Check")
            ArabImage.image = UIImage(named: "Uncheck1" )
            self.navigationItem.title = "Setting"
        }
        else{
            EngImage.image = UIImage(named: "Uncheck1")
            ArabImage.image = UIImage(named: "Check" )
            self.navigationItem.title = "إعدادات"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: tabCondition)
    }
    
    @IBAction func languge_method (sender: AnyObject) -> Void {
        if(sender.tag == 1) {
            EngImage.image = UIImage(named: "Check")
            ArabImage.image = UIImage(named: "Uncheck1" )
            selectedLang = english
        } else {
            EngImage.image = UIImage(named: "Uncheck1")
            ArabImage.image = UIImage(named: "Check" )
            selectedLang = arabic
        }
    }
    
    @IBAction func saveMethod() -> Void {
        UserDefaults.standard.set(notification, forKey: Notification_on)
        let  str = UserDefaults.standard.object(forKey: lang) as! String
        print(selectedLang)
        if selectedLang != str  {
            UserDefaults.standard.set(selectedLang, forKey: lang)
            let mainStoryBoard : UIStoryboard!
            if selectedLang == arabic {
                mainStoryBoard = UIStoryboard(name: "Main_ar", bundle: nil)
            }
            else{
                mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            }
            
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "revalvc") as! SWRevealViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.set(rootViewController: vc)
        }
    }
    
    @IBAction func langSwitch(_ sender : UISwitch) -> Void {
        if sender.isOn {
            notification = true
        }
        else{
            notification = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UIWindow {
    

    func set(rootViewController newRootViewController: UIViewController, withTransition transition: CATransition? = nil) {
        
        let previousViewController = rootViewController
        
        if let transition = transition {
            // Add the transition
            layer.add(transition, forKey: kCATransition)
        }
        
        rootViewController = newRootViewController
        
        // Update status bar appearance using the new view controllers appearance - animate if needed
        if UIView.areAnimationsEnabled {
            UIView.animate(withDuration: CATransaction.animationDuration()) {
                newRootViewController.setNeedsStatusBarAppearanceUpdate()
            }
        } else {
            newRootViewController.setNeedsStatusBarAppearanceUpdate()
        }
        
        /// The presenting view controllers view doesn't get removed from the window as its currently transistioning and presenting a view controller
        if let transitionViewClass = NSClassFromString("UITransitionView") {
            for subview in subviews where subview.isKind(of: transitionViewClass) {
                subview.removeFromSuperview()
            }
        }
        if let previousViewController = previousViewController {
            // Allow the view controller to be deallocated
            previousViewController.dismiss(animated: false) {
                // Remove the root view in case its still showing
                previousViewController.view.removeFromSuperview()
            }
        }
    }
}

