//
//  MenuViewController.swift
//  Fitq8
//
//  Created by Manish Kumar on 12/26/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController , UITableViewDataSource , UITableViewDelegate,UIGestureRecognizerDelegate{

    @IBOutlet var tableView: UITableView!
    let home = "الرئيسية"
    let category = "الفئة"
    let my_cart = "سلتي"
    let my_account = "حسابي"
    let my_orders = "طلباتي"
    let my_address = "عنواني التوصيل"
    let sign_out = "تسجيل خروج"
    let MY_List = "قائمتي"
    let setting = "إعدادات"
    
    var view1: UIView!
    var window : UIWindow!
 
    var Arr = NSMutableArray()
    var imgArr = NSMutableArray()
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
        self.UpdateData()
       Utility().setStatusBarBackgroundColor(color: UIColor(red: 30/255.0, green: 45/255.0, blue: 54/255.0, alpha: 1.0))
        UINavigationBar.appearance().barTintColor = UIColor(red: 0, green: 0/255, blue: 205/255, alpha: 1)
        // Do any additional setup after loading the view.
     }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        window = UIApplication.shared.keyWindow
        let singleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap))
        singleFingerTap.delegate=self
        view1=UIView(frame: CGRect(x: 260,y: 0,width: self.revealViewController().frontViewController.view.frame.size.width, height: self.revealViewController().frontViewController.view.frame.size.height))
        view1.addGestureRecognizer(singleFingerTap)
        window.addSubview(view1)
        
       self.UpdateData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     view1.isHidden = true
    }
    
    func handleSingleTap() -> Void {
        self.revealViewController().revealToggle(animated: true)
    }
    
    func UpdateData() -> Void {
        tableView.delegate=self
        tableView.dataSource=self
        
        let  usersession = UserSession().getUserSession()
        let language = UserDefaults.standard.object(forKey: lang) as! String
        if usersession.userId != nil {
            if language == english {
                Arr = ["Home","Categories","My Cart","My List","My Account","My orders","My Addresses","Sign out","Setting"]//,"Profile"
            }
            else{
                Arr = [home,category,my_cart,MY_List,my_account,my_orders,my_address,sign_out,setting]//,"Profile"
            }
            imgArr = ["store_menu","menu_cat","cart_menu","heart","account_menu","MyOrder","My_address_menu","signout_menu","setting_menu"]//,"profile_menu"
        }
        else{
            if language == english {
                Arr = ["Home","Categories","My Cart","My List","My Account","Setting"]
            }
            else{
                Arr = [home,category,my_cart,MY_List,my_account,setting]
            }
            
            imgArr = ["store_menu","menu_cat","cart_menu","heart","account_menu","setting_menu"]
        }
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Arr.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        
    let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
//        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let lbl : UIImageView? = cell.contentView.viewWithTag(2) as? UIImageView
        lbl?.image = UIImage.init(named: imgArr[indexPath.row] as! String)
        
        let lbl2 : UILabel! = cell.contentView.viewWithTag(1) as? UILabel
        lbl2?.text=Arr[indexPath.row] as? String
        
        let cartarray = CoredataClass().getCartData() as NSArray
        let crtlbl : UILabel! = cell.contentView.viewWithTag(3) as? UILabel
        crtlbl.layer.cornerRadius = crtlbl.frame.size.height/2
        crtlbl.clipsToBounds = true
        crtlbl.textAlignment=NSTextAlignment.center
        crtlbl.font = UIFont.systemFont(ofSize: 10)
        crtlbl?.text="\(UInt(cartarray.count))"
        crtlbl.isHidden = true
        if indexPath.row == 2 {
            crtlbl.isHidden = false
        }
        
        let linelbl : UILabel! = cell.contentView.viewWithTag(4) as? UILabel
        linelbl.isHidden = false
        if indexPath.row == Arr.count-1 {
            linelbl.isHidden = true
        }
        
        return cell
        }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        if indexPath.row == 0{
            self.revealViewController().revealToggle(animated: true)
            if UserDefaults.standard.integer(forKey: SelectedTab) != 0 || !UserDefaults.standard.bool(forKey: tabCondition){
                UserDefaults.standard.set(0, forKey: SelectedTab)
                let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! TabBarVC
                self.revealViewController().setFront(viewcontroller, animated: false)
            }
        }
        else if indexPath.row == 1 {
            self.revealViewController().revealToggle(animated: true)
            if UserDefaults.standard.integer(forKey: SelectedTab) != 1 || !UserDefaults.standard.bool(forKey: tabCondition){
                UserDefaults.standard.set(1, forKey: SelectedTab)
                let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! TabBarVC
                self.revealViewController().setFront(viewcontroller, animated: false)
            }
        }
        else if indexPath.row == 2 {
            self.revealViewController().revealToggle(animated: true)
            if UserDefaults.standard.integer(forKey: SelectedTab) != 2 || !UserDefaults.standard.bool(forKey: tabCondition){
                UserDefaults.standard.set(2, forKey: SelectedTab)
                let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! TabBarVC
                self.revealViewController().setFront(viewcontroller, animated: false)
            }
        }
        else if indexPath.row == 3 {
            self.revealViewController().revealToggle(animated: true)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Favorite") as! UINavigationController
            self.revealViewController().frontViewController.present(vc, animated: true, completion: nil)
            
        }
        else if indexPath.row == 4 {
            self.revealViewController().revealToggle(animated: true)
            if UserDefaults.standard.integer(forKey: SelectedTab) != 3 || !UserDefaults.standard.bool(forKey: tabCondition){
                UserDefaults.standard.set(3, forKey: SelectedTab)
                let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! TabBarVC
                self.revealViewController().setFront(viewcontroller, animated: false)
            }
        }
    
        else if indexPath.row == 5 {
            let Usersession = UserSession().getUserSession()
            if Usersession.userId == nil {
                self.revealViewController().revealToggle(animated: true)
                if UserDefaults.standard.integer(forKey: SelectedTab) != 4 || !UserDefaults.standard.bool(forKey: tabCondition){
                    UserDefaults.standard.set(4, forKey: SelectedTab)
                    let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! TabBarVC
                    self.revealViewController().setFront(viewcontroller, animated: false)
                }
            }
            else{
                /// show My orders
                self.revealViewController().revealToggle(animated: true)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyoredrViewController") as! UINavigationController
                self.revealViewController().frontViewController.present(vc, animated: true, completion: nil)
                
            }
            
        }
        else if indexPath.row == 6 {
            
            self.revealViewController().revealToggle(animated: true)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyaddressViewController") as! UINavigationController
            self.revealViewController().frontViewController.present(vc, animated: true, completion: nil)
            
        }

        else if indexPath.row == 7 {
            UserSession().clearUserSession()
            self.revealViewController().revealToggle(animated: true)
            UserDefaults.standard.set(3, forKey: SelectedTab)
            let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! TabBarVC
            self.revealViewController().setFront(viewcontroller, animated: false)
        }
        else if indexPath.row == 8 {
            
            self.revealViewController().revealToggle(animated: true)
            if UserDefaults.standard.integer(forKey: SelectedTab) != 4 || !UserDefaults.standard.bool(forKey: tabCondition){
                UserDefaults.standard.set(4, forKey: SelectedTab)
                let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! TabBarVC
                self.revealViewController().setFront(viewcontroller, animated: false)
            }
        }
    }
    
    
      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
