//
//  CarttabelVC_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 18/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class CarttabelVC_ar: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var cartTabel : UITableView!
    @IBOutlet var orderTotallbl : UILabel!
    @IBOutlet var deliveryCharge : UILabel!
    @IBOutlet var totalAmount : UILabel!
    @IBOutlet var orderNowBtn : UIButton!
    @IBOutlet var amountMainView : UIView!
    @IBOutlet var noRecordlbl : UILabel!
    @IBOutlet var yourOrderLbl : UILabel!
    var  ProductList = NSMutableArray()
    var orderTotalPrice : Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "سلتي"
        noRecordlbl.text = No_record_ar
        cartTabel.delegate = self
        cartTabel.dataSource = self
        self.automaticallyAdjustsScrollViewInsets = false
        
//        var frame = amountMainView.frame
//        frame.origin.y = cartTabel.frame.origin.y+cartTabel.frame.size.height+5
//        amountMainView.frame = frame
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        cartTabel.register(UINib(nibName: "cartcell_ar", bundle: nil), forCellReuseIdentifier: "cell")
        ProductList = CoredataClass().getCartData().mutableCopy() as! NSMutableArray
        cartTabel.reloadData()
        noRecordlbl.isHidden = true
        self.getOrderTotalAmount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:CustumCell = cartTabel.dequeueReusableCell(withIdentifier: "cell") as! CustumCell!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        
        let cart_item = ProductList.object(at: indexPath.row) as! Cart
        
        cell.storeName?.text = cart_item.product_name_ar
        
        cell.priceLbl?.text = String(format: "KD %0.2f", (Float(cart_item.price!)!)*Float(cart_item.quentity!)!)
        
        cell.Quentitylbl?.text="\(cart_item.quentity!)"
        
        cell.cartIncreaseBtn?.addTarget(self, action: #selector(self.Increase_decreaseMethod), for: .touchUpInside)
        cell.cartDecreaeseBtn?.addTarget(self, action: #selector(self.Increase_decreaseMethod), for: .touchUpInside)
        
        cell.storeImage?.setimage(link: cart_item.product_image)
        
        cell.Rateview?.notSelectedImage = UIImage.init(named: star_empty)
        cell.Rateview?.halfSelectedImage = UIImage.init(named: star_Half)
        cell.Rateview?.fullSelectedImage = UIImage.init(named: star_Full)
        cell.Rateview?.editable = false;
        cell.Rateview?.maxRating = 5;
        cell.Rateview?.midMargin = 0;
        cell.Rateview?.isUserInteractionEnabled = false;
        var rating : Float = 0.0
        if cart_item.rating?.isEmpty == false {
            let datadic = (cart_item.rating!).objectFromJSONString() as! NSDictionary
            let totalRatingCount = (datadic.object(forKey: "totalstar") as! NSString).floatValue
            let baseRating = (datadic.object(forKey: "totalrecord") as! NSString).floatValue
            rating = totalRatingCount/baseRating
        }
        cell.Rateview?.rating = rating
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            let cart_item = ProductList.object(at: indexPath.row) as! Cart
            moc.delete(cart_item)
            do {
                try moc.save()
            } catch {
                print(error)
            }
            ProductList.removeObject(at: indexPath.row)
            tableView.reloadData()
            getOrderTotalAmount()
        }
    }
    
    func Increase_decreaseMethod(_ sender : UIButton) -> Void {
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: cartTabel)
        let indexPath = cartTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let cart_item = ProductList.object(at: indexPath.row) as! Cart
        
        if sender.tag == 1 {
            if UInt(cart_item.quentity!) == 10 || UInt(cart_item.quentity!) == UInt(cart_item.product_quantity!) {
                return
            }
            else{
                let queintity = UInt(cart_item.quentity!)!+1
                cart_item.quentity = "\(queintity)"
                do {
                    try moc.save()
                } catch {
                    print(error)
                }
                cartTabel.reloadData()
                getOrderTotalAmount()
            }
        }
        else{
            if UInt(cart_item.quentity!) == 1 {
                return
            }
            else{
                let queintity = UInt(cart_item.quentity!)!-1
                cart_item.quentity = "\(queintity)"
                do {
                    try moc.save()
                } catch {
                    print(error)
                }
                cartTabel.reloadData()
                getOrderTotalAmount()
            }
        }
    }
    
    
    
    func getOrderTotalAmount() -> Void {
        
        if ProductList.count>0 {
            cartTabel.isHidden=false
            amountMainView.isHidden=false
            yourOrderLbl.isHidden=false
            noRecordlbl.isHidden = true
            var total = 0 as Float
            var deliveryfee = 0 as Float
            
            for i in 0..<ProductList.count {
                
                let cartdata = ProductList.object(at: i) as! Cart
                total = total+(Float(cartdata.price! as String)!)*(Float(cartdata.quentity! as String)!)
                deliveryfee = Float(cartdata.delivery_price! as String)!
            }
            orderTotalPrice = total
            orderTotallbl.text = String(format: "KD %0.2f", total)
            deliveryCharge.text = String(format: "KD %0.2f", deliveryfee)
            totalAmount.text = String(format: "KD %0.2f", total+deliveryfee)
        }
        else{
            orderNowBtn.setTitle("إضافة منتج", for: .normal)
            cartTabel.isHidden=true
            amountMainView.isHidden=true
            yourOrderLbl.isHidden=true
            noRecordlbl.isHidden = false
        }
    }
    
    
    @IBAction func moveToaddress() -> Void {
        if ProductList.count>0 {
            let cartdata = ProductList.object(at: 0) as! Cart
            let min_order = Float(cartdata.min_order! as String)! as Float
            if min_order > orderTotalPrice{
                let str = String(format: "أقل قيمة للطلب من هذا المطعم  %0.2f KD", min_order)
                let alert = DXAlertView(title: nil, contentText: str, leftButtonTitle: nil, rightButtonTitle: "OK")
                alert?.show()
            }
            else {
                let Usersession = UserSession().getUserSession()
                if Usersession.userId == nil {
                    let alert = DXAlertView(title: nil, contentText: "تريد إكمال عملية الشراء كعضو أو كزائر",
                                            leftButtonTitle: "زائر", rightButtonTitle: "عضو")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        UserDefaults.standard.set(true, forKey: "loginFrom_another")
                        UserDefaults.standard.set(true, forKey: "Forcart")
                        let user = self.storyboard?.instantiateViewController(withIdentifier: "user") as! UINavigationController
                        self.present(user, animated: true, completion: nil)
                    }
                    alert?.leftBlock = {() -> Void in
                        self.performSegue(withIdentifier: "addaddress", sender: self)
                    }
                }
                else{
                    self.performSegue(withIdentifier: "address", sender: self)
                }
            }
        }
        else{
            UserDefaults.standard.set(0, forKey: SelectedTab)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "revalvc") as! SWRevealViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.set(rootViewController: vc)
        }
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.bool(forKey: "login_forCart") {
            UserDefaults.standard.set(false, forKey: "login_forCart")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "address") as! SelectAddressVC_ar
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        ////////////  add side bar button ////
        Utility().AddSidePanel(VC: self)
        
        UserDefaults.standard.set(self.tabBarController?.selectedIndex, forKey: SelectedTab)
        UserDefaults.standard.set(true, forKey: tabCondition)
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: tabCondition)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
