//
//  AddAddressVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 06/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class AddAddressVC: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet var segment : UISegmentedControl?
    var  addressTable : UITableView!
    var  AddressArray = NSMutableArray()
    var wihthoutAccLbl : UILabel?
    
    @IBOutlet var scrollview : UIScrollView!
    
    @IBOutlet var addAddressView : UIView?
    
    @IBOutlet var conTOpaymentView : UIView?
    
    var area_selectView : UIView?
    var area_selectLbl : UILabel?
    var selectedArea = ""
    
    var nametextF : UITextField?
    var addressNameTextF : UITextField?
    var PhoneNotextF : UITextField?
    var BlockNotextF : UITextField?
    var StreettextF : UITextField?
    var HousetextF : UITextField?
    var FloortextF : UITextField?
    var ExtraTextF : UITextField?
    var jaddaTextF : UITextField?
    
    var namelbl : UILabel?
    var addressNamelbl: UILabel?
    var arealbl: UILabel?
    var phonelbl : UILabel?
    var blocklbl : UILabel?
    var streetlbl: UILabel?
    var houselbl : UILabel?
    var floorlbl : UILabel?
    var extralbl: UILabel?
    var jaddalbl : UILabel?
    
    @IBOutlet var saveAddBtn : UIButton?
    
    var Is_editAdd : Bool = false
    var Is_add : Bool = false
    
    var  address : Address!
    var first_Con : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.AddressCustom_Mthod()
        let singleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap))
        singleFingerTap.delegate=self
        scrollview.addGestureRecognizer(singleFingerTap)

        self.navigationItem.title="Add Address"
        nametextF?.delegate=self
        PhoneNotextF?.delegate=self
        BlockNotextF?.delegate=self
        StreettextF?.delegate=self
        HousetextF?.delegate=self
        FloortextF?.delegate=self
        ExtraTextF?.delegate=self
        addressNameTextF?.delegate=self
        jaddaTextF?.delegate=self
        var frame = segment?.frame
        frame?.size.height = 40
        segment?.frame=frame!
        
        var frame1 = addAddressView?.frame
        frame1?.origin.y = (segment?.frame.origin.y)!+(segment?.frame.size.height)!
        addAddressView?.frame = frame1!
        addressTable = UITableView(frame: CGRect(x: self.view.frame.size.width, y: (addAddressView?.frame.origin.y)!+20, width: self.view.frame.size.width, height: self.view.frame.size.height-(addAddressView?.frame.origin.y)! - 20), style: .plain)
        addressTable.separatorStyle = .none
        addressTable.register(UINib.init(nibName: "addressCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.view.addSubview(addressTable)
        saveAddBtn?.layer.cornerRadius = 3
        saveAddBtn?.layer.borderColor = UIColor.blue.cgColor
        saveAddBtn?.layer.borderWidth = 1
        
        wihthoutAccLbl = UILabel(frame: (segment?.frame)!)
        wihthoutAccLbl?.text = "Check out without Account"
        wihthoutAccLbl?.textColor = UIColor.black
        wihthoutAccLbl?.font = UIFont.systemFont(ofSize: 16)
        wihthoutAccLbl?.textAlignment = NSTextAlignment.center
        self.view.addSubview(wihthoutAccLbl!)
    
        
        let Usersession = UserSession().getUserSession()
        if (Usersession.userId != nil) {
            
            if Is_editAdd {
                segment?.isHidden = true
                wihthoutAccLbl?.isHidden = false
                wihthoutAccLbl?.text = "Update Address"
                self.navigationItem.title="Update Address"
                conTOpaymentView?.isHidden = true
            }
            else{
                segment?.isHidden = false
                wihthoutAccLbl?.isHidden = true
                
            }
            nametextF?.isHidden=true
            namelbl?.isHidden=true
        }
        else{
            segment?.isHidden = true
            wihthoutAccLbl?.isHidden = false
            saveAddBtn?.isHidden = true
            
            addressNameTextF?.isHidden=true
            addressNamelbl?.isHidden=true
        }
        
        let area = CoredataClass().getAreaFromAreaId(UserDefaults.standard.object(forKey: area_id) as! String) as Area
        area_selectLbl?.text = area.area_name
        selectedArea = area.area_id!
        
    }
    
    @IBAction func segControleButtonMethod(_ sender : Any){
        if (segment?.selectedSegmentIndex == 0){
            saveAddBtn?.isHidden = false
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                var frame = self.addAddressView?.frame
                frame?.origin.x = 0
                self.addAddressView?.frame = frame!
                
                var frame2 = self.addressTable.frame
                frame2.origin.x = self.view.frame.size.width
                self.addressTable?.frame = frame2
                
                self.conTOpaymentView?.isHidden = false
                
                
                }, completion: nil)
            
        }
        else{
            self.view.endEditing(true)
            if first_Con {
                first_Con = false
                self.GetAdd_Method()
            }
            saveAddBtn?.isHidden = true
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                var frame = self.addAddressView?.frame
                frame?.origin.x = -self.view.frame.size.width
                self.addAddressView?.frame = frame!
                
                var frame2 = self.addressTable.frame
                frame2.origin.x = 0
                self.addressTable?.frame = frame2
                
                self.conTOpaymentView?.isHidden = true
                
                }, completion: nil)
        }
    }
    
    func AddressCustom_Mthod() {
        
        namelbl = UILabel(frame: CGRect(x: 20, y: 10, width:view.frame.size.width-40 , height: 15 ))
        namelbl?.text = "Name"
        namelbl?.font = UIFont.systemFont(ofSize: 10)
        scrollview.addSubview(namelbl!)
        
        nametextF = UITextField(frame: CGRect(x:20, y: (namelbl?.frame.origin.y)!+(namelbl?.frame.size.height)!, width: view.frame.size.width-40, height: 30))
        scrollview.addSubview(nametextF!)
        nametextF?.font = UIFont.systemFont(ofSize: 14)
        nametextF?.backgroundColor = UIColor.white
        nametextF?.tintColor = UIColor.blue
        self.addLine((nametextF?.frame)!)
        
        addressNamelbl = UILabel(frame: CGRect(x: 20, y: 10, width:view.frame.size.width-40 , height: 15 ))
        addressNamelbl?.text = "Address name"
        addressNamelbl?.font = UIFont.systemFont(ofSize: 10)
        scrollview.addSubview(addressNamelbl!)
        
        addressNameTextF = UITextField(frame: CGRect(x:20, y: (addressNamelbl?.frame.origin.y)!+(addressNamelbl?.frame.size.height)!, width: view.frame.size.width-40, height: 30))
        scrollview.addSubview(addressNameTextF!)
        addressNameTextF?.font = UIFont.systemFont(ofSize: 14)
        addressNameTextF?.backgroundColor = UIColor.white
        addressNameTextF?.tintColor = UIColor.blue
        self.addLine((addressNameTextF?.frame)!)
        
        arealbl = UILabel(frame: CGRect(x: 20, y: (nametextF?.frame.origin.y)!+(nametextF?.frame.height)!+10, width: view.frame.size.width-40, height: 15))
        arealbl?.font = UIFont.systemFont(ofSize: 10)
        arealbl?.text = "Area"
        scrollview.addSubview(arealbl!)

        
        area_selectLbl = UILabel(frame: CGRect(x: 20, y: (arealbl?.frame.origin.y)!+(arealbl?.frame.height)!, width: view.frame.size.width-40, height: 30))
        area_selectLbl?.backgroundColor = UIColor.white
        area_selectLbl?.font = UIFont.systemFont(ofSize: 14)
        scrollview.addSubview(area_selectLbl!)
        self.addLine((area_selectLbl?.frame)!)
        
        
        phonelbl = UILabel(frame: CGRect(x: 20, y: (area_selectLbl?.frame.origin.y)!+(area_selectLbl?.frame.height)!+10, width: view.frame.width-40, height: 15))
        phonelbl?.text = "Phone No."
        phonelbl?.font = UIFont.systemFont(ofSize: 10)
        scrollview.addSubview(phonelbl!)
        
        PhoneNotextF = UITextField(frame: CGRect(x: 20, y: (phonelbl?.frame.origin.y)!+(phonelbl?.frame.height)!, width: view.frame.width-40, height: 30))
        PhoneNotextF?.backgroundColor = UIColor.white
        PhoneNotextF?.font = UIFont.systemFont(ofSize: 14)
        PhoneNotextF?.tintColor = UIColor.blue
        PhoneNotextF?.keyboardType = .numberPad
        scrollview.addSubview(PhoneNotextF!)
        self.addLine((PhoneNotextF?.frame)!)
        
        blocklbl = UILabel(frame: CGRect(x: 20, y: (PhoneNotextF?.frame.origin.y)!+(PhoneNotextF?.frame.height)!+10, width: view.frame.size.width-40, height: 15))
        blocklbl?.font = UIFont.systemFont(ofSize: 10)
        blocklbl?.text = "Block No"
        scrollview.addSubview(blocklbl!)
        
        BlockNotextF = UITextField(frame: CGRect(x: 20, y: (blocklbl?.frame.origin.y)!+(blocklbl?.frame.height)!, width: view.frame.width-40, height: 30))
        BlockNotextF?.backgroundColor = UIColor.white
        BlockNotextF?.font = UIFont.systemFont(ofSize: 14)
        BlockNotextF?.tintColor = UIColor.blue
        scrollview.addSubview(BlockNotextF!)
        self.addLine((BlockNotextF?.frame)!)
        
        streetlbl = UILabel(frame: CGRect(x: 20, y: (BlockNotextF?.frame.origin.y)!+(BlockNotextF?.frame.height)!+10, width: view.frame.size.width-40, height: 15))
        streetlbl?.text = "Street"
        streetlbl?.font = UIFont.systemFont(ofSize: 10)
        scrollview.addSubview(streetlbl!)
        
        StreettextF = UITextField(frame: CGRect(x: 20, y: (streetlbl?.frame.origin.y)!+(streetlbl?.frame.height)!, width: view.frame.width-40, height: 30))
        StreettextF?.backgroundColor = UIColor.white
        StreettextF?.font = UIFont.systemFont(ofSize: 14)
        StreettextF?.tintColor = UIColor.blue
        scrollview.addSubview(StreettextF!)
         self.addLine((StreettextF?.frame)!)
        
        
        houselbl = UILabel(frame: CGRect(x: 20, y: (StreettextF?.frame.origin.y)!+StreettextF!.frame.height+10, width: view.frame.width-40, height: 15))
        houselbl?.font = UIFont.systemFont(ofSize: 10)
        houselbl?.text = "House/Building No."
        scrollview.addSubview(houselbl!)
        
        HousetextF = UITextField(frame: CGRect(x: 20, y: (houselbl?.frame.origin.y)!+(houselbl?.frame.height)!, width: view.frame.width-40, height: 30))
        HousetextF?.backgroundColor = UIColor.white
        HousetextF?.font = UIFont.systemFont(ofSize: 14)
        HousetextF?.tintColor = UIColor.blue
        scrollview.addSubview(HousetextF!)
        self.addLine((HousetextF?.frame)!)
        
        floorlbl = UILabel(frame: CGRect(x: 20, y: (HousetextF?.frame.origin.y)!+(HousetextF?.frame.height)!+10, width: view.frame.size.width-40, height: 15))
        floorlbl?.font = UIFont.systemFont(ofSize: 10)
        floorlbl?.text = "Floor(Optional)"
        scrollview.addSubview(floorlbl!)
        
        FloortextF = UITextField(frame: CGRect(x: 20, y: (floorlbl?.frame.origin.y)!+(floorlbl?.frame.height)!, width: view.frame.width-40, height: 30))
        FloortextF?.font = UIFont.systemFont(ofSize: 14)
        FloortextF?.backgroundColor = UIColor.white
        FloortextF?.tintColor = UIColor.blue
        scrollview.addSubview(FloortextF!)
        self.addLine((FloortextF?.frame)!)
    
        
        jaddalbl = UILabel(frame: CGRect(x: 20, y: (FloortextF?.frame.origin.y)!+(FloortextF?.frame.height)!+10, width: view.frame.size.width-40, height: 15))
        jaddalbl?.font = UIFont.systemFont(ofSize: 10)
        jaddalbl?.text = "Jadda(Optional)"
        scrollview.addSubview(jaddalbl!)
        
        jaddaTextF = UITextField(frame: CGRect(x: 20, y: (jaddalbl?.frame.origin.y)!+(jaddalbl?.frame.height)!, width: view.frame.width-40, height: 30))
        jaddaTextF?.backgroundColor = UIColor.white
        jaddaTextF?.font = UIFont.systemFont(ofSize: 14)
        jaddaTextF?.tintColor = UIColor.blue
        scrollview.addSubview(jaddaTextF!)
        self.addLine((jaddaTextF?.frame)!)
        
        extralbl = UILabel(frame: CGRect(x: 20, y: (jaddaTextF?.frame.origin.y)!+(jaddaTextF?.frame.height)!+10, width: view.frame.width-40, height: 15))
        extralbl?.font = UIFont.systemFont(ofSize: 10)
        extralbl?.text = "Extra info (Optional)"
        scrollview.addSubview(extralbl!)
        
        
        ExtraTextF = UITextField(frame: CGRect(x: 20, y: (extralbl?.frame.origin.y)!+(extralbl?.frame.height)!, width: view.frame.width-40, height: 30))
        ExtraTextF?.backgroundColor = UIColor.white
        ExtraTextF?.font = UIFont.systemFont(ofSize: 14)
         ExtraTextF?.tintColor = UIColor.blue
        scrollview.addSubview(ExtraTextF!)
        self.addLine((ExtraTextF?.frame)!)
        
        scrollview?.contentSize = CGSize(width: self.view.frame.size.width, height: (ExtraTextF?.frame.origin.y)!+(ExtraTextF?.frame.size.height)!+20)
        
    }
    
    func addLine(_ frame : CGRect) -> Void {
        
        let line = UILabel(frame: CGRect(x: frame.origin.x-1, y: frame.origin.y+frame.size.height, width: frame.size.width+3, height: 1))
        line.backgroundColor = UIColor.lightGray
        scrollview.addSubview(line)
        
        let leftline = UILabel(frame: CGRect(x: frame.origin.x-1, y: frame.origin.y+frame.size.height-5, width: 1, height: 5))
        leftline.backgroundColor = UIColor.lightGray
        scrollview.addSubview(leftline)
        
        let Rightline = UILabel(frame: CGRect(x:frame.origin.x+frame.size.width+1, y: frame.origin.y+frame.size.height-5, width: 1, height: 5))
        Rightline.backgroundColor = UIColor.lightGray
        scrollview.addSubview(Rightline)
        
        
    }
    
    
    func CheckTextFeilds() -> Bool {
        
        let Usersession = UserSession().getUserSession()
        if (Usersession.userId == nil && ((nametextF?.text?.isEmpty)! == true)) {
            let alert = DXAlertView(title: nil, contentText: "Please enter your name", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.nametextF?.becomeFirstResponder()
            }
        }
        else if (Usersession.userId != nil && ((addressNameTextF?.text?.isEmpty) == true)) {
            let alert = DXAlertView(title: nil, contentText: "Please insert the Address name (ex. Home , office etc)", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.addressNameTextF?.becomeFirstResponder()
            }
        }
        else if ((PhoneNotextF?.text?.isEmpty)! == true || (PhoneNotextF?.text?.characters.count)!<8) {
            let alert = DXAlertView(title: nil, contentText: "Please Enter valid Phone no.", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.PhoneNotextF?.becomeFirstResponder()
            }
        }
        else if ((BlockNotextF?.text?.isEmpty)! == true) {
            let alert = DXAlertView(title: nil, contentText: "Please insert block no.", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.BlockNotextF?.becomeFirstResponder()
            }
        }
        else if ((StreettextF?.text?.isEmpty)! == true) {
            let alert = DXAlertView(title: nil, contentText: "Please insert street", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.StreettextF?.becomeFirstResponder()
            }
        }
        else if ((HousetextF?.text?.isEmpty)! == true) {
            let alert = DXAlertView(title: nil, contentText: "Please insert house no.", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.HousetextF?.becomeFirstResponder()
            }
        }
        else {
            return true
        }
        return false
    }
    
    @IBAction func Cont_To_Payment() -> Void {
        if self.CheckTextFeilds() {

            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            let add = NSEntityDescription.insertNewObject(forEntityName: "Address", into: moc) as! Address
            add.address_name = addressNameTextF?.text
            add.area_id = UserDefaults.standard.object(forKey: area_id) as? String
            add.block = BlockNotextF?.text
            add.extra_delivery_information = ExtraTextF?.text
            add.floor = FloortextF?.text
            add.jadda = jaddaTextF?.text
            add.phone = PhoneNotextF?.text
            add.street_num = StreettextF?.text
            add.house_num = HousetextF?.text
            
            let chackoutVC = self.storyboard?.instantiateViewController(withIdentifier: "chackout") as! CheckoutViewController
            chackoutVC.address = add
            let usersession = UserSession().getUserSession()
            if usersession.userId == nil {
                chackoutVC.fullName = nametextF?.text
            }
            self.navigationController?.pushViewController(chackoutVC, animated: true)

        }
    }
    
    
    @IBAction func Saveaddress_method()-> Void {
        if CheckTextFeilds() {
            if Is_editAdd {
                if Is_add {
                    self.SaveAddress()
                }
                else{
                    self.UpdateAddress()
                }
            }
            else{
                self.SaveAddress()
            }
        }
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        var frame = self.view.frame
        
        if (textField == PhoneNotextF) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y -= 30
                self.view.frame = frame
                }, completion: nil)
        } else if (textField == BlockNotextF || textField == StreettextF) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y -= 100
                self.view.frame = frame
                }, completion: nil)
        }
        else if (textField == HousetextF || textField == FloortextF ) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y -= 150
                self.view.frame = frame
                }, completion: nil)
        }
        else if ( textField  == jaddaTextF || textField == ExtraTextF) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y -= 200
                self.view.frame = frame
                }, completion: nil)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var frame = self.view.frame
        
        if (textField == PhoneNotextF) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y += 30
                self.view.frame = frame
                }, completion: nil)
        } else if (textField == BlockNotextF || textField == StreettextF) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y += 100
                self.view.frame = frame
                }, completion: nil)
        }
        else if (textField == HousetextF || textField == FloortextF ) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y += 150
                self.view.frame = frame
                }, completion: nil)
        }
        else if ( textField  == jaddaTextF || textField == ExtraTextF) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y += 200
                self.view.frame = frame
                }, completion: nil)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (PhoneNotextF?.text?.characters.count)! + string.characters.count - range.length
        if textField == PhoneNotextF {
            var value = true
            if string.hasPrefix(" ") {
                value = false
            }
            if range.location == 0 || range.location == 1 {
                if string.hasPrefix("0") || string.hasPrefix(" ") {
                    value = false
                }
            }
            if range.length + range.location > (PhoneNotextF?.text?.characters.count)! {
                value = false
            }
            return ((newLength <= 8) && value) ? true : false
        }
        return true
        
    }
    
    
    
    func SaveAddress() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let user = UserSession().getUserSession()
            
            let parameters = ["apikey":API_KEY,
                              "user_id":user.userId! as String,
                              "area_id":selectedArea as String,
                              "street_num": (StreettextF?.text)!  as String,
                              "phone":(PhoneNotextF?.text)! as String,
                              "block":(BlockNotextF?.text)! as String,
                              "house_num":(HousetextF?.text)! as String,
                              "lat":"1.00033",
                              "lon":"2.02652",
                              "address_name": (addressNameTextF?.text)! as String,
                              "extra_delivery_information":(ExtraTextF?.text)! as String,
                              "jadda": (jaddaTextF?.text)! as String,
                              "floor": (FloortextF?.text)! as String
                
            ]
            
            
            Alamofire.request(SAVE_ADD_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        let alert = DXAlertView(title: nil, contentText: "Address is saved", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                            if self.Is_add {
                                self.navigationController!.popViewController(animated: true)
                            }
                        }
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: nil, rightButtonTitle: "Ok")
                    alert?.show()
                }
                
            })
        }
        else{
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
            
        }
        
    }
    
    
    func UpdateAddress() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "address_id":address.address_id! as String,
                              "area_id":selectedArea as String,
                              "street_num": (StreettextF?.text)!  as String,
                              "phone":(PhoneNotextF?.text)! as String,
                              "block":(BlockNotextF?.text)! as String,
                              "house_num":(HousetextF?.text)! as String,
                              "lat":"1.00033",
                              "lon":"2.02652",
                              "address_name": (addressNameTextF?.text)! as String,
                              "extra_delivery_information":(ExtraTextF?.text)! as String,
                              "jadda": (jaddaTextF?.text)! as String,
                              "floor": (FloortextF?.text)! as String
                
            ]
            
            Alamofire.request(UPDATE_ADD_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        self.navigationController!.popViewController(animated: true)
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: nil, rightButtonTitle: "Ok")
                    alert?.show()
                }
                
            })
        }
        else{
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
            
        }
        
    }
    
    
    
    func GetAdd_Method() -> Void {
        if  Utility().hasInternetConnection() {
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            let user = UserSession().getUserSession()
            
            let parameters = ["apikey":API_KEY,
                              "user_id": user.userId! as String]
            
            Alamofire.request(GET_ADD_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        CoredataClass().DeleteAllData("Address")
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        for i in 0..<dicEntry.count  {
                            let dictCat = dicEntry.object(at: i) as! NSDictionary
                            print(dictCat)
                            
                            let className = NSStringFromClass(Address.self) as String
                            let entity = NSEntityDescription.entity(forEntityName:className, in: moc)!
                            let cat = (NSManagedObject(entity: entity, insertInto: moc) as! Address)
                            cat.safeSetValuesForKeys(with: dictCat as Any? as? [AnyHashable: Any] ?? [:] )
                            
                            do {
                                try moc.save()
                            } catch {
                                print(error)
                            }
                        }
                        self.AddressArray = CoredataClass().getAddressData().mutableCopy() as! NSMutableArray
                        self.addressTable.delegate = self
                        self.addressTable.dataSource = self
                        self.addressTable.reloadData()
                        
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetAdd_Method()
                    }
                    
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    
    
    /////////// Table view delegate datasource method////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.AddressArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
    }
    
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CustumCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustumCell!
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let add = AddressArray.object(at: indexPath.row) as! Address
        let area = CoredataClass().getAreaFromAreaId(add.area_id!) as Area
        
        cell.addresnamelbl?.text = "\(add.address_name!)"
        cell.arealbl?.text = "\(area.area_name!)"
        cell.blocklbl?.text = "Block : \(add.block!)"
        cell.streetlbl?.text = "Street : \(add.street_num!)"
        cell.houselbl?.text = "House No.: \(add.house_num!)"
        cell.phoneNolbl?.text = "Phone No.: \(add.phone!)"
        
        return cell
        
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        let address = AddressArray.object(at: indexPath.row) as! Address
        let areaId = UserDefaults.standard.object(forKey: area_id) as? String
        let area = CoredataClass().getAreaFromAreaId(areaId! as String) as Area
        if areaId == address.area_id {
            let chackoutVC = self.storyboard?.instantiateViewController(withIdentifier: "chackout") as! CheckoutViewController
            chackoutVC.address = address
            self.navigationController?.pushViewController(chackoutVC, animated: true)
        }
        else{
            let str = "Please select address in \(area.area_name! as String)"
            let alert = DXAlertView(title: nil, contentText:str, leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
        
    }
    
    
    func handleSingleTap() -> Void {
        self.view.endEditing(true)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(false, forKey: tabCondition)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
