//
//  CheckoutViewController_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 18/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class CheckoutViewController_ar: UIViewController,UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet var  CartyItem: UITableView?
    @IBOutlet var PaySummaryVIew : UIView?
    @IBOutlet var PaymentView : UIView?
    @IBOutlet var CashMainView : UIView?
    @IBOutlet var CashInnerView : UIView?
    @IBOutlet var knetMainView : UIView?
    @IBOutlet var knetInnerview : UIView?
    @IBOutlet var cashImageview : UIImageView?
    @IBOutlet var knetImageview : UIImageView?
    @IBOutlet var cashLbl : UILabel?
    @IBOutlet var knetLbl : UILabel?
    @IBOutlet var orderTotal : UILabel?
    @IBOutlet var deliveryFee : UILabel?
    @IBOutlet var totalAmount : UILabel?
    var delFee : Float?
    var  ProductList = NSMutableArray()
    var address : Address!
    var fullName : String!
    var totalPrice : Float = 0.0
    var store_id : String = ""
    var isCash = true
    
    var  CheckArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        CartyItem?.delegate=self
        CartyItem?.dataSource=self
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.title = "إكمال عملية الشراء"
        // Do any additional setup after loading the view.
        CartyItem?.register(UINib(nibName: "Checkout_ar", bundle: nil), forCellReuseIdentifier: "cell")
        ProductList = CoredataClass().getCartData().mutableCopy() as! NSMutableArray
        self.getOrderTotalAmount()
        
        self.addPosition()

        
    }
    
    
    func addPosition() -> Void {
        var frame = PaySummaryVIew?.frame
        frame?.origin.y = (CartyItem?.frame.origin.y)!+(CartyItem?.frame.size.height)!+5
        PaySummaryVIew?.frame = frame!
        
        var frame1 = PaymentView?.frame
        frame1?.origin.y = (PaySummaryVIew?.frame.origin.y)!+(PaySummaryVIew?.frame.size.height)!
        PaymentView?.frame = frame1!
        
        cashLbl?.textColor = UIColor.blue
        
        CashMainView?.layer.cornerRadius = 5
        CashMainView?.layer.borderColor = UIColor.blue.cgColor
        CashMainView?.layer.borderWidth = 1
        
        knetMainView?.layer.cornerRadius = 5
        knetMainView?.layer.borderColor = UIColor.blue.cgColor
        knetMainView?.layer.borderWidth = 1
        knetMainView?.isHidden = true
        
        CashInnerView?.layer.cornerRadius = 3
        CashInnerView?.layer.borderColor = UIColor.init(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
        CashInnerView?.layer.borderWidth = 1
        
        knetInnerview?.layer.cornerRadius = 3
        knetInnerview?.layer.borderColor = UIColor.init(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
        knetInnerview?.layer.borderWidth = 1
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ProductList.count
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        
        let cell:CustumCell = CartyItem!.dequeueReusableCell(withIdentifier: "cell") as! CustumCell!
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let cart_item = ProductList.object(at: indexPath.row) as! Cart
        
        cell.storeName?.text = cart_item.product_name_ar
        
        cell.priceLbl?.text = String(format: "KD %0.2f", (Float(cart_item.price!)!)*Float(cart_item.quentity!)!)
        cell.Quentitylbl?.text="\("كمية : ")\(cart_item.quentity!)"
        
        cell.storeImage?.setimage(link: cart_item.product_image)
        
        cell.Rateview?.notSelectedImage = UIImage.init(named: star_empty)
        cell.Rateview?.halfSelectedImage = UIImage.init(named: star_Half)
        cell.Rateview?.fullSelectedImage = UIImage.init(named: star_Full)
        cell.Rateview?.editable = false;
        cell.Rateview?.maxRating = 5;
        cell.Rateview?.midMargin = 0;
        cell.Rateview?.isUserInteractionEnabled = false;
        var rating : Float = 0.0
        if cart_item.rating?.isEmpty == false {
            let datadic = (cart_item.rating!).objectFromJSONString() as! NSDictionary
            let totalRatingCount = (datadic.object(forKey: "totalstar") as! NSString).floatValue
            let baseRating = (datadic.object(forKey: "totalrecord") as! NSString).floatValue
            rating = totalRatingCount/baseRating
        }
        cell.Rateview?.rating = rating
        return cell
        
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        if indexPath.row == 0{
            
        }
    }
    
    func getOrderTotalAmount() -> Void {
        var total = 0 as Float
        for i in 0..<ProductList.count {
            let cartdata = ProductList.object(at: i) as! Cart
            total = total+Float(cartdata.price!)!*Float(cartdata.quentity!)!
            delFee = Float(cartdata.delivery_price! as String)
            store_id = cartdata.store_id! as String
        }
        
        if delFee!>Float(0) {
            deliveryFee?.text = String(format: "KD %0.2f", delFee!)
        }
        else{
            deliveryFee?.text = "Free"
        }
        orderTotal?.text = String(format: "KD %0.2f", total)
        totalAmount?.text = String(format: "KD %0.2f", total+delFee!)
        totalPrice = total+delFee!
        print(totalPrice)
        print(delFee)
        
    }    
    
    //////////////// create json data /////////////
    func getUserDetailJsonData() -> String {
        
        let usersession = UserSession().getUserSession()
        var user_id : String = ""
        var email : String = ""
        var name : String = ""
        
        if usersession.userId != nil {
            user_id = usersession.userId!
            email = usersession.email!
            name = usersession.userName!
        }
        else{
            name = fullName
            user_id = "0"
        }
        
        let userdetail = ["address_name" : address.address_name! as String,
                          "area_id" : address.area_id! as String,
                          "block" : address.block! as String,
                          "street" : address.street_num! as String,
                          "house_no" : address.house_num! as String,
                          "phone" : address.phone! as String,
                          "extra_delivery_info" : address.extra_delivery_information! as String,
                          "client_id" : user_id,
                          "email" : email,
                          "fullname" : name,
                          ]
        
        var string: String = ""
        let jsonData: Data? = try! JSONSerialization.data(withJSONObject: userdetail, options: .prettyPrinted)
        if jsonData != nil {
            string = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue) as! String
            print(string)
        }
        
        return string
    }
    
    
    func getOrderDetailJsonData() -> String {
        
        let array = CoredataClass().getCartData() as NSArray
        let dataArray = NSMutableArray()
        for i in 0..<array.count {
            let cartItem = array.object(at: i) as! Cart
            let userdetail = ["product_id": cartItem.product_id! as String,
                              "store_id": cartItem.store_id! as String,
                              "product_name":cartItem.product_name! as String,
                              "productname_ar":cartItem.product_name_ar! as String,
                              "product_price": cartItem.price! as String,
                              "qty":cartItem.quentity! as String,
                              ] as [String : Any]
            
            dataArray.add(userdetail)
        }
        
        var string: String = ""
        let jsonData: Data? = try! JSONSerialization.data(withJSONObject: dataArray, options: .prettyPrinted)
        if jsonData != nil {
            string = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue) as! String
            
        }
        print(string)
        return string
    }
    
    
    func PlaceOrder() -> Void {
        
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let order_array = self.getOrderDetailJsonData()
            let user_detail = self.getUserDetailJsonData()
            print(order_array)
            print(user_detail)
            var paymentType = ""
            if isCash {
                paymentType = "Cash"
            }
            else{
                paymentType = "knet"
            }
            
            let parameters = ["apikey":API_KEY,
                              "order_array": order_array,
                              "user_detail":user_detail,
                              "payment_type":paymentType as String,
                              "delivery_charge":"\(delFee!)",
                "total":"\(totalPrice)",
                "store_id":store_id as String
            ]
            print(parameters)
            
            Alamofire.request(ORDER_PLACE, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        if self.isCash {
                            let dic = jsonObject.object(forKey: "data") as! NSDictionary
                            let order_Id = dic.object(forKey: "order_id") as! String
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "thankYou") as! ThankYouVC
                            vc.order_id = order_Id
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else {
                            let datadic = jsonObject.object(forKey: "data") as! NSDictionary
                            let dic =  (jsonObject.object(forKey: "response") as! String).objectFromJSONString() as! NSDictionary
                            let knet = self.storyboard?.instantiateViewController(withIdentifier: "Knet") as! KnetViewController
                            knet.Urlstr = dic.object(forKey: "PaymentURL") as! String
                            knet.orderId = datadic.object(forKey: "order_id") as! String
                            self.navigationController?.pushViewController(knet, animated: true)
                            
                        }
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed_ar, leftButtonTitle: nil, rightButtonTitle: "Ok")
                    alert?.show()
                    
                }
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,
            leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
        
    }
    
    
    @IBAction func Cash_button_Method() -> Void {
        cashLbl?.textColor = UIColor.blue
        knetLbl?.textColor = UIColor.black
        knetMainView?.isHidden = true
        CashMainView?.isHidden = false
        isCash = true
    }
    
    @IBAction func Knet_button_Method() -> Void {
        cashLbl?.textColor = UIColor.black
        knetLbl?.textColor = UIColor.blue
        knetMainView?.isHidden = false
        CashMainView?.isHidden = true
        isCash = false
    }
    
    @IBAction func orderNowBtn() -> Void{
        //        self.performSegue(withIdentifier: "thankyou", sender: self)
        self.PlaceOrder()
    }
    
    
    @IBAction func BackButton() -> Void {
        self.navigationController!.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
