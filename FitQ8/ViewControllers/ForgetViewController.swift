//
//  ForgetViewController.swift
//  FitQ8
//
//  Created by Manish Kumar on 1/3/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class ForgetViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet var Submitbtn:UIButton?
    @IBOutlet var emailtext :UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title="Forgot Password"
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func SubmitBtn(sender : AnyObject){
        if !isValidEmail(testStr:(emailtext?.text)!){
            let alert = DXAlertView(title: nil, contentText: "Please Enter Your Correct mail Id", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.emailtext?.becomeFirstResponder()
            }
        }
        else {
                        self.Forgetmethod()
        }
    }
    
    func Forgetmethod() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "user_email":(emailtext?.text)! as String
            ]
            Alamofire.request(FORGOT_PASS_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status")as! Bool == true) {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "checkcode") as! ChackCodeVC
                        vc.email_id = (self.emailtext?.text)! as String
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    else{
                       
                        let str = "We are unable to find account with \((self.emailtext?.text)! as String) Email id." as String
                        let alert = DXAlertView(title: nil, contentText: str, leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                        }
                    }
                    
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: nil, rightButtonTitle: "Ok")
                    alert?.show()
                    
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    

    
    @IBAction func BackButton() -> Void {

        self.navigationController!.popViewController(animated: true)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
