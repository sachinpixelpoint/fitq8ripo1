//
//  KnetViewController.swift
//  FitQ8
//
//  Created by Manish Kumar on 17/03/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit

class KnetViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet var webView : UIWebView!
    var Urlstr : String! = ""
    var orderId : String! = ""
    var hud : MBProgressHUD!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Payment"
        webView.delegate = self
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDModeIndeterminate
        hud.labelText = "Loading..."
        self.view.addSubview(hud)
        let url = NSURL (string: Urlstr);
        let requestObj = NSURLRequest(url: url! as URL);
        webView.loadRequest(requestObj as URLRequest);
        
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        //Check here if still webview is loding the content
        if webView.isLoading {
            return
        }
 
        print("Webview loding finished")
        hud.removeFromSuperview()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url: URL? = request.url
        print("The Redirected URL is: \(url)")
        let myString: String? = url?.absoluteString
        if (myString?.contains("http://fitq8.com/webservice/knetsucess"))! {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "thankYou") as! ThankYouVC
            vc.order_id = orderId;
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return true
    }
    
    
    @IBAction func BackButtonMethod() -> Void {
        self.navigationController!.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
