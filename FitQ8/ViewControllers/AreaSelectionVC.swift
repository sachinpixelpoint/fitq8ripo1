//
//  AreaSelectionVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 28/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class AreaSelectionVC: UIViewController {
    
    @IBOutlet var areaMainView : UIView!
    @IBOutlet var AreaLbl : UILabel!
    @IBOutlet var areaBtn : UIButton!
    @IBOutlet var openBtn : UIButton!
    @IBOutlet var mapview : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility().setStatusBarBackgroundColor(color: UIColor(red: 30/255.0, green: 45/255.0, blue: 54/255.0, alpha: 1.0))
        // Do any additional setup after loading the view.
        
        mapview.layer.masksToBounds = true
        mapview.layer.cornerRadius = mapview.frame.size.height/2
        
        AreaLbl.text = "Select Area"  //عبد الله السالم
        openBtn.isHighlighted = true
        openBtn.isUserInteractionEnabled = false
        
        self.GetArea_Method()
        
    }
    
////////////////  GET AREA WEB SERVICE///////////////////
    //////////////////////////////////////////////////////////
    func GetArea_Method() -> Void {
        if  Utility().hasInternetConnection() {
            let moc = (UIApplication.shared.delegate
as! AppDelegate).persistentContainer.viewContext
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,]
            
            Alamofire.request(Get_Area_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        CoredataClass().DeleteAllData("Area")
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        for i in 0..<dicEntry.count  {
                            let dictCat = dicEntry.object(at: i) as! NSDictionary
                            print(dictCat)
                            
                            let className = NSStringFromClass(Area.self) as String
                            let entity = NSEntityDescription.entity(forEntityName:className, in: moc)!
                            let cat = (NSManagedObject(entity: entity, insertInto: moc) as! Area)
                            cat.safeSetValuesForKeys(with: dictCat as Any? as? [AnyHashable: Any] ?? [:] )
                            
                            do {
                                try moc.save()
                            } catch {
                                print(error)
                            }
                        }
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetArea_Method()
                    }
                }
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(false, forKey: "areaChanged")
        if UserDefaults.standard.object(forKey: area_id) as? String != nil {
            let area = CoredataClass().getAreaFromAreaId(UserDefaults.standard.object(forKey: area_id) as! String) as Area
            AreaLbl.text = area.area_name
            openBtn.isHighlighted = false
            openBtn.isUserInteractionEnabled = true
            UserDefaults.standard.set(true, forKey: firstLaunch)
        }
    }
    
    @IBAction func selectAreaMethod() -> Void{
        
        let areavc = self.storyboard?.instantiateViewController(withIdentifier: "areavc") as! UINavigationController
        self.present(areavc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func OpenFitQ8BtnMethod() -> Void{
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "revalvc") as! SWRevealViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
