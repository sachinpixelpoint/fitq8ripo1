//
//  CategoryVC.swift
//  FitQ8
//
//  Created by Manish Kumar on 04/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire


class CategoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet var categoryTabel : FZAccordionTableView!
    var catArray = NSMutableArray()
    var catItemArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryTabel.allowMultipleSectionsOpen = true
        categoryTabel.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCellReuseIdentifier")
        categoryTabel.register(UINib.init(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.title = "Category"

        self.GetCategory()
        
    }
    
    
    func GetCategory() -> Void {
        if  Utility().hasInternetConnection() {
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY ]
            Alamofire.request(CAT_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        CoredataClass().DeleteAllData("Category")
                        let dataAr = NSMutableArray()
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        for i in 0..<dicEntry.count  {
                            let dictCat = dicEntry.object(at: i) as! NSDictionary
                            print(dictCat)
                            
                            let className = NSStringFromClass(Category.self) as String
                            let entity = NSEntityDescription.entity(forEntityName:className, in: moc)!
                            let cat = (NSManagedObject(entity: entity, insertInto: moc) as! Category)
                            cat.safeSetValuesForKeys(with: dictCat as Any? as? [AnyHashable: Any] ?? [:] )
                            dataAr.add(cat)
                            do {
                                try moc.save()
                            } catch {
                                print(error)
                            }
                        }
                        print(dataAr)
                        self.catArray = CoredataClass().getCategoryData().mutableCopy() as! NSMutableArray
                        self.categoryTabel?.delegate = self
                        self.categoryTabel?.dataSource = self
                        self.categoryTabel?.reloadData()
                    }
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetCategory()
                    }
                    
                }
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    
    
    ///////////////// Table view methods ///////////////
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let category = catArray.object(at: section) as! Category
        return (CoredataClass().getCategoryItemData(category.category_id!) as NSArray).count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return catArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView(tableView, heightForHeaderInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CustumCell = categoryTabel.dequeueReusableCell(withIdentifier: "CategoryCell") as! CustumCell!
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let category = catArray.object(at: indexPath.section) as! Category
        let catItem = CoredataClass().getCategoryItemData(category.category_id!) as NSArray
        let item = catItem.object(at: indexPath.row) as! Category
        
        cell.storeName?.text = item.category_name
        cell.lineLbl?.isHidden = false
        if catItem.count <= 1 {
            cell.lineLbl?.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = FZAccordionTableViewHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        
        let colorVIew = UIView(frame: CGRect(x: 0, y: 1, width: tableView.frame.size.width, height: 48))
        colorVIew.backgroundColor = UIColor.white       //UIColor.init(red: 0/255, green: 0/255, blue: 205/255, alpha: 1)
        /* Create custom view to display section header... */
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.frame.size.width-15, height: 50))
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(18))
        
        let category = catArray.object(at: section) as! Category
        let itemar = CoredataClass().getCategoryItemData(category.category_id!) 
        
        
        label.text = category.category_name
        label.textAlignment = .left
        label.textColor = UIColor.black
        colorVIew.addSubview(label)
        label.sizeToFit()
        var lblframe = label.frame
        lblframe.origin.y = 25-lblframe.size.height/2
        label.frame = lblframe
        
        let itemNobl = UILabel(frame: CGRect(x: label.frame.origin.x+label.frame.size.width+10, y: 10, width: 30, height: 30))
        itemNobl.backgroundColor = UIColor.init(red: 189/255, green: 16/255, blue: 0/255, alpha: 1)
        itemNobl.textColor = UIColor.white
        itemNobl.font = UIFont.systemFont(ofSize: 14)
        itemNobl.layer.masksToBounds = true
        itemNobl.layer.borderColor = UIColor.white.cgColor
        itemNobl.layer.borderWidth=2
        itemNobl.text = "\(UInt(itemar.count))"
        itemNobl.textAlignment = NSTextAlignment.center
        itemNobl.sizeToFit()
        var frame = itemNobl.frame
        frame.size.width = frame.size.width+20
//        frame.size.height = frame.size.height+5
        frame.origin.y = 25-frame.size.height/2
        itemNobl.frame = frame
        itemNobl.layer.cornerRadius = itemNobl.frame.size.height/2
        colorVIew.addSubview(itemNobl)
       
        
        if lblframe.size.width > colorVIew.frame.size.width-(30+itemNobl.frame.size.width) {
            var lblframe1 = label.frame
            lblframe1.size.width = colorVIew.frame.size.width-(30+itemNobl.frame.size.width)
            label.frame = lblframe1
            itemNobl.frame =  CGRect(x: label.frame.origin.x+label.frame.size.width+10, y: itemNobl.frame.origin.y, width: itemNobl.frame.size.width, height: itemNobl.frame.size.height)
        }
        
        
        view.addSubview(colorVIew)
        return view
    }
    
    // FZAccordionTableViewDelegate////// -
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView) {
    }

    ///////////////////// DID selecct method //////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = catArray.object(at: indexPath.section) as! Category
        let catItem = CoredataClass().getCategoryItemData(category.category_id!) as NSArray
        let item = catItem.object(at: indexPath.row) as! Category
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "productVC") as! ProductViewController
        vc.category = item
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        /////////// Add left panel button /////
        Utility().AddSidePanel(VC: self)
        
        UserDefaults.standard.set(self.tabBarController?.selectedIndex, forKey: SelectedTab)
        UserDefaults.standard.set(true, forKey: tabCondition)
        
        let cartarray = CoredataClass().getCartData() as NSArray
        
        let cartBtn = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(40), height: CGFloat(40)))
        cartBtn.setImage(UIImage(named: "cart_icon")!, for: .normal)
        cartBtn.addTarget(self, action: #selector(self.CartbuttonItem), for: .touchUpInside)
        if cartarray.count > 0 {
            let cartitelbl = UILabel(frame: CGRect(x: CGFloat(30), y: CGFloat(2), width: CGFloat(15), height: CGFloat(8)))
            cartitelbl.font = UIFont.systemFont(ofSize: 8)
            cartitelbl.backgroundColor = UIColor.white
            cartitelbl.clipsToBounds = true
            cartitelbl.layer.cornerRadius = cartitelbl.frame.size.height / 2
            cartitelbl.textAlignment = .center
            cartitelbl.textColor = UIColor.blue
            cartitelbl.text = "\(UInt(cartarray.count))"
            cartBtn.addSubview(cartitelbl)
        }
        let cartbtn = UIBarButtonItem(customView: cartBtn)
        
        self.navigationItem.rightBarButtonItem = cartbtn
    }
    
    
    func CartbuttonItem() -> Void{
        self.tabBarController?.selectedIndex = 2
    }
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(false, forKey: tabCondition)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
