//
//  SignupViewController.swift
//  FitQ8
//
//  Created by Manish Kumar on 1/2/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class SignupViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet var fnametext :UITextField?
    @IBOutlet var emailtext :UITextField?
    @IBOutlet var passtext :UITextField?
    @IBOutlet var passtext1:UITextField?
    @IBOutlet var  contacttext :UITextField?
    @IBOutlet var Signup:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fnametext?.delegate = self
        passtext1?.delegate = self
        passtext?.delegate = self
        emailtext?.delegate = self
        contacttext?.delegate = self

        self.navigationItem.title="Sign Up"
    }
    

    
    
    @IBAction func signupBtn(sender : AnyObject) {
        if ((fnametext?.text?.isEmpty)! == true){
            let alert = DXAlertView(title: nil, contentText: "Please enter Full Name", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.fnametext?.becomeFirstResponder()
            }
        }else if !isValidEmail(testStr:(emailtext?.text)!) {
            let alert = DXAlertView(title: nil, contentText: "Please enter your Valid Email ID", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.emailtext?.becomeFirstResponder()
            }
        }else if((contacttext?.text?.isEmpty)! == true || (contacttext?.text?.characters.count)!<8){
            let alert = DXAlertView(title: nil, contentText: "Please enter Phone Number", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.contacttext?.becomeFirstResponder()
            }
            
        }else if((passtext?.text?.isEmpty)! == true || (passtext?.text?.characters.count)!<6){

            let alert = DXAlertView(title: nil, contentText: "Please enter minimum 6 Digit Password", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
                self.passtext?.becomeFirstResponder()
            }
        }
        else if (passtext?.text != passtext1?.text){
            let alert = DXAlertView(title: nil, contentText: "Password Doesn't Match", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            alert?.rightBlock = {() -> Void in
            }
        }
        else {
            
            self.signupmethod()
            
        }
    }
    
    
    func signupmethod() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "fname":(fnametext?.text)! as String,
                              "user_email":(emailtext?.text)! as String,
                              "pass":(passtext?.text)! as String,
                              "contactno":(contacttext?.text)! as String
            ]
            Alamofire.request(SIGNUP_URL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status")as! Bool == true) {
                        let dict = jsonObject.object(forKey: "data") as! NSDictionary
                        let Usersession = UserSession()
                        Usersession.userId = dict.object(forKey: "userid") as? String
                        Usersession.userName = (self.fnametext?.text)! as String
                        Usersession.email = (self.emailtext?.text)! as String
//                        Usersession.password = dict.object(forKey: "") as? String
//                        Usersession.PhotoUrl = dict.object(forKey: "") as? String
//                        Usersession.gender = dict.object(forKey: "gender") as? String
                        Usersession.phoneNo = (self.contacttext?.text)! as String
                        UserSession().SetUserSession(Usersession)
                        if UserDefaults.standard.bool(forKey: "loginFrom_another") {
                            UserDefaults.standard.set(false, forKey: "loginFrom_another")
                            if UserDefaults.standard.bool(forKey: "Forcart") {
                                UserDefaults.standard.set(false, forKey: "Forcart")
                                UserDefaults.standard.set(true, forKey: "login_forCart")
                            }
                            self.dismiss(animated: true, completion: nil)
                        }
                        else{
                            self.navigationController!.popToRootViewController(animated: true)
                        }

    
                    }
                    else{
                        let alert = DXAlertView(title: nil, contentText: "An account already exists with this Email address.Please try to login.", leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                        }
                    }
                    
                    
                    
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: nil, rightButtonTitle: "Ok")
                    alert?.show()
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        var frame = self.view.frame
        
        if textField == self.passtext {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y -= 50
                self.view.frame = frame
                }, completion: nil)
        } else if (textField == self.passtext1) {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y -= 50
                self.view.frame = frame
                }, completion: nil)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var frame = self.view.frame
        if textField == self.passtext {
            UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                frame.origin.y += 50
                self.view.frame = frame
                }, completion: nil)
        }
        else if (textField == self.passtext1) { var frame = self.view.frame
            if textField == self.passtext1 {
                UIView.animate(withDuration: 0.25, delay: 0, animations: { () -> Void in
                    frame.origin.y += 50
                    self.view.frame = frame
                    }, completion: nil)
                
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (contacttext?.text?.characters.count)! + string.characters.count - range.length
        if textField == contacttext {
            var value = true
            if string.hasPrefix(" ") {
                value = false
            }
            if range.location == 0 || range.location == 1 {
                if string.hasPrefix("0") || string.hasPrefix(" ") {
                    value = false
                }
            }
            if range.length + range.location > (contacttext?.text?.characters.count)! {
                value = false
            }
            return ((newLength <= 8) && value) ? true : false
        }
        return true
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
        
    }
    
}

