import UIKit
import Alamofire

class ProductDetailVC: UIViewController,UIGestureRecognizerDelegate {
    
    var product : Product!
    
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var addTocartView : UIView!
    
    var topView : UIView!
    var bottomView : UIView!
    
    var productImage : UIImageView!
    var productnameLbl : UILabel!
    var productPriceLbl : UILabel!
    var cartLbl : UILabel!
    var heartBtn : UIButton!
    var RedheartBtn : UIButton!
    var heartFrame0 : CGRect!
    var heartFrame1 : CGRect!
    var cartBtn : UIButton!
    
    
    var productDetailBtn : UIButton!
    var userGuideBtn : UIButton!
    var buyingBtn : UIButton!
    var suppInfoBtn : UIButton!
    
    var productDetailLbl : UILabel!
    var userGuideLbl : UILabel!
    var buyingLbl : UILabel!
    var suppInfoLbl : UILabel!
    
    var descriptionTextview : UITextView!
    
    @IBOutlet var quintityLbl : UILabel!
    
    var sizeView : UIView!
    var sizeLbl : UILabel!
    var custumSizeView : UIView!
    var sizeArr = NSArray()
    var productPrice : Float = 0.0
    var singleTap : UITapGestureRecognizer!
    
    var view1: UIView!
    var blackOverlay : UIView!
    var window : UIWindow!
    var quentityView : UIView!
    var selectedQuantity = 1
    var price : Float = 0.0
    var delivery_price : String = ""
    var min_order : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custumDesign()
        // Do any additional setup after loading the view.
        if product.price != nil {
            sizeArr = (product.price)?.objectFromJSONString() as! NSArray
            if sizeArr.count>0 {
                let sizedic = sizeArr.object(at: 0) as! NSDictionary
                sizeLbl.text = sizedic.object(forKey: "sizename") as! String?
                productPrice = NSString(string: sizedic.object(forKey: "price") as! String).floatValue
            }
        }
        
        let listItems = CoredataClass().GetMyListItemWithProductID(product.product_id!)
        if listItems.count > 0 {
            RedheartBtn.frame = heartFrame1
        }
        
        print(productPrice)
        productPriceLbl.text = "KD \(productPrice)"
        self.navigationItem.title = product.product_name
        
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-50)
        self.view.addSubview(scrollView)
        self.view.addSubview(addTocartView)
        singleTap = UITapGestureRecognizer(target: self, action: #selector(self.scrollTapgesture))
        singleTap.delegate=self
        scrollView.addGestureRecognizer(singleTap)
        custumSizeView = UIView()
        
//     if sizeArr.count <= 1 {
//            custumSizeView.isHidden = true
//        }
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: bottomView.frame.origin.y+bottomView.frame.size.height)
        let cartarray = CoredataClass().getCartData() as NSArray
        cartLbl.layer.masksToBounds = true
        cartLbl.text =  "\(UInt(cartarray.count))"
        quintityLbl.text = "Qty: 1"
        selectedQuantity = 1
        cartLbl.layer.cornerRadius = cartLbl.frame.size.height/2
        productDetailBtn.setTitleColor(UIColor.blue, for: .normal)
        descriptionTextview.text = product.product_description
        productDetailLbl.isHidden = false
        userGuideLbl.isHidden = true
        buyingLbl.isHidden = true
        suppInfoLbl.isHidden = true
        
        productImage.setimage(link: product.product_image)
        productImage.contentMode = .scaleAspectFit
        productnameLbl.text = product.product_name
        
        self.GetDelivery_Price()
        
    }
    
    func GetDelivery_Price() -> Void {
        if  Utility().hasInternetConnection() {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading
            self.view.addSubview(hud)
            self.view.isUserInteractionEnabled=false
            
            let parameters = ["apikey":API_KEY,
                              "store_id":product.store_id! as String,
                              "area_id":UserDefaults.standard.object(forKey: area_id) as! String]
            print(parameters)
            Alamofire.request(DELIVERY_PRICE, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                    
                        let dic = jsonObject.object(forKey: "data") as! NSDictionary
                        self.delivery_price = dic.object(forKey: "deliveryprice") as! String
                        self.min_order = dic.object(forKey: "min_order") as! String
                        
                    }
                
                case .failure(let error):
                    hud.removeFromSuperview()
                    self.view.isUserInteractionEnabled=true
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed, leftButtonTitle: nil, rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetDelivery_Price()
                    }
                    
                }
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet, leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }

    
    /////////////////////  custum design method ////////////
    func  custumDesign() -> Void {
        topView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 292))
        topView.backgroundColor = UIColor.white
        scrollView.addSubview(topView)

        let mainWidth = self.view.frame.size.width
        productImage = UIImageView(frame: CGRect(x: 0, y: 0, width: mainWidth, height: 200))
        topView.addSubview(productImage)
        
        productnameLbl = UILabel(frame: CGRect(x: 15, y: 210, width: mainWidth-30, height: 40))
        productnameLbl.font = UIFont.systemFont(ofSize: 14)
        productnameLbl.numberOfLines = 0
        topView.addSubview(productnameLbl)
        
        productPriceLbl = UILabel(frame: CGRect(x: 15, y: 250, width: (mainWidth/2)-15, height: 40))
        productPriceLbl.font = UIFont.systemFont(ofSize: 14)
        topView.addSubview(productPriceLbl)
        
        let W = mainWidth/10
        
        heartFrame0 = CGRect(x: W*7.5+W/2, y: 15+W/2, width: 0, height: 0)
        heartFrame1 = CGRect(x: W*7.5, y: 15, width: W, height: W)
        heartBtn = UIButton(frame: heartFrame1)
        heartBtn.setImage(UIImage.init(named: "heart"), for: .normal)
        heartBtn.addTarget(self, action: #selector(self.LikeButtonMethod(_:)), for: .touchUpInside)
        topView.addSubview(heartBtn)
        
        RedheartBtn = UIButton(frame: heartFrame0)
        RedheartBtn.setImage(UIImage.init(named: "heart_red"), for: .normal)
        RedheartBtn.addTarget(self, action: #selector(self.LikeButtonMethod(_:)), for: .touchUpInside)
        topView.addSubview(RedheartBtn)
        
        cartBtn = UIButton(frame: CGRect(x: W*8.5, y: 15, width: W, height: W))
        cartBtn.setImage(UIImage.init(named: "cart_menu"), for: .normal)
        cartBtn.addTarget(self, action: #selector(self.CartbuttonItem), for: .touchUpInside)
        topView.addSubview(cartBtn)
        
        cartLbl = UILabel(frame: CGRect(x: W*9.5-10, y: 15, width: 20, height: 20))
        cartLbl.backgroundColor = UIColor.init(red:16/255 , green: 0/255, blue: 182/255, alpha: 1)
        cartLbl.font = UIFont.systemFont(ofSize: 9)
        cartLbl.textAlignment = NSTextAlignment.center
        cartLbl.textColor = UIColor.white
        topView.addSubview(cartLbl)
        
        sizeView = UIView(frame: CGRect(x: mainWidth/2, y: 250, width: (mainWidth/2)-W, height: 40))
        topView.addSubview(sizeView)
        sizeLbl = UILabel(frame: CGRect(x: 0, y: 0, width: sizeView.frame.size.width-30, height: 40))
        sizeLbl.font = UIFont.systemFont(ofSize: 14)
        sizeLbl.textAlignment = NSTextAlignment.center
        sizeView.addSubview(sizeLbl)
        
        let downImg = UIImageView(frame: CGRect(x: sizeLbl.frame.size.width+5, y: 13, width: 15, height: 15))
        downImg.image=UIImage.init(named: "Down_gray")
        sizeView.addSubview(downImg)
        
        let  sizebtn = UIButton(frame: CGRect(x: 0, y: 0, width: sizeView.frame.size.width, height: sizeView.frame.size.height))
        sizebtn.addTarget(self, action: #selector(self.sizeBtnMethod), for: .touchUpInside)
        sizebtn.backgroundColor = UIColor.clear
        sizeView.addSubview(sizebtn)
        
        let line = UILabel(frame: CGRect(x: 0, y: 290, width: mainWidth, height: 2))
        line.backgroundColor = UIColor.lightGray
        topView.addSubview(line)
        
        
        bottomView = UIView(frame: CGRect(x: 0, y: topView.frame.origin.y+topView.frame.size.height+10, width: self.view.frame.size.width, height: 170))
        bottomView.backgroundColor = UIColor.white
        scrollView.addSubview(bottomView)
        
        let width = (self.view.frame.size.width-10)/10
        productDetailBtn = UIButton(frame: CGRect(x: 5, y: 5, width: width*3, height: 40))
        productDetailBtn.addTarget(self, action: #selector(self.productDetailMethod), for: .touchUpInside)
        productDetailBtn.setTitle("Product detail", for: .normal)
        productDetailBtn.titleLabel?.textAlignment = NSTextAlignment.center
        productDetailBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        bottomView.addSubview(productDetailBtn)
        
        productDetailLbl = UILabel(frame : CGRect(x: 5, y: productDetailBtn.frame.origin.y+productDetailBtn.frame.size.height, width: productDetailBtn.frame.size.width, height: 1))
        productDetailLbl.backgroundColor = UIColor.lightGray
        bottomView.addSubview(productDetailLbl)
        
        userGuideBtn = UIButton(frame: CGRect(x: productDetailBtn.frame.origin.x+productDetailBtn.frame.size.width, y: 5, width: width*2, height: 40))
        userGuideBtn.addTarget(self, action: #selector(self.UserGuideMethod), for: .touchUpInside)
        userGuideBtn.setTitle("User guide", for: .normal)
        userGuideBtn.setTitleColor(UIColor.black, for: .normal)
        userGuideBtn.titleLabel?.textAlignment = NSTextAlignment.center
        userGuideBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        bottomView.addSubview(userGuideBtn)
        
        userGuideLbl = UILabel(frame : CGRect(x: productDetailLbl.frame.origin.x+productDetailLbl.frame.size.width, y: userGuideBtn.frame.origin.y+userGuideBtn.frame.size.height, width: userGuideBtn.frame.size.width, height: 1))
        userGuideLbl.backgroundColor = UIColor.lightGray
        bottomView.addSubview(userGuideLbl)
        
        buyingBtn = UIButton(frame: CGRect(x: userGuideBtn.frame.origin.x+userGuideBtn.frame.size.width, y: 5, width: width*2, height: 40))
        buyingBtn.addTarget(self, action: #selector(self.buyingMethod), for: .touchUpInside)
        buyingBtn.setTitle("Buying", for: .normal)
        buyingBtn.setTitleColor(UIColor.black, for: .normal)
        buyingBtn.titleLabel?.textAlignment = NSTextAlignment.center
        buyingBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        bottomView.addSubview(buyingBtn)
        
        buyingLbl = UILabel(frame : CGRect(x:  userGuideLbl.frame.origin.x+userGuideLbl.frame.size.width, y: buyingBtn.frame.origin.y+buyingBtn.frame.size.height, width: buyingBtn.frame.size.width, height: 1))
        buyingLbl.backgroundColor = UIColor.lightGray
        bottomView.addSubview(buyingLbl)
        
        suppInfoBtn = UIButton(frame: CGRect(x: buyingBtn.frame.origin.x+buyingBtn.frame.size.width, y: 5, width: width*3, height: 40))
        suppInfoBtn.addTarget(self, action: #selector(self.suppInfoMethod), for: .touchUpInside)
        suppInfoBtn.setTitle("Supplement info", for: .normal)
        suppInfoBtn.setTitleColor(UIColor.black, for: .normal)
        suppInfoBtn.titleLabel?.textAlignment = NSTextAlignment.center
        suppInfoBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        bottomView.addSubview(suppInfoBtn)
        
        suppInfoLbl = UILabel(frame : CGRect(x: buyingLbl.frame.origin.x+buyingLbl.frame.size.width, y: suppInfoBtn.frame.origin.y+suppInfoBtn.frame.size.height, width: suppInfoBtn.frame.size.width, height: 1))
        suppInfoLbl.backgroundColor = UIColor.lightGray
        bottomView.addSubview(suppInfoLbl)
        
        descriptionTextview = UITextView(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width-20, height: 100))
        descriptionTextview.isEditable = false
        bottomView.addSubview(descriptionTextview)
        
    }
    
    //////////////////////////// save into cart ///////////////////
    @IBAction func saveTocartMethod() -> Void {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        if CheckPrevioutStore() {
            let cart = NSEntityDescription.insertNewObject(forEntityName: "Cart", into: moc) as! Cart
            cart.price = "\(productPrice)"
            cart.product_id = product.product_id
            cart.product_name = product.product_name
            cart.product_name_ar = product.product_name_ar
            cart.product_des = product.product_description
            cart.product_image = product.product_image
            cart.store_id = product.store_id
            cart.quentity = "\(selectedQuantity)"
            cart.rating = product.rating
            cart.product_quantity = product.product_quantity
            cart.delivery_price = delivery_price
            cart.min_order = min_order
            print(cart)
            
            do {
                try moc.save()
            } catch {
                print(error)
            }
            let alert = DXAlertView(title: nil, contentText: "Item is added into cart", leftButtonTitle: nil, rightButtonTitle: "OK")
            alert?.show()
            self.updateItemQuentity()
            self.setCartRIghtbarbutton()
        }
        else{
            let cartar = CoredataClass().getCartData() as NSArray
            if cartar.count>0 {
                let cart = cartar.object(at: 0) as! Cart
                let storear = CoredataClass().getStorewithStoreID(cart.store_id! as String) as NSArray
                if storear.count > 0 {
                    let store = storear.object(at: 0) as! Store
                    let alert = DXAlertView(title: nil, contentText: "There are items in your cart from \(store.store_name! as String). Do you want to clear your Cart", leftButtonTitle: "NO", rightButtonTitle: "YES")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        CoredataClass().DeleteAllData("Cart")
                        self.saveTocartMethod()
                    }
                }
            }
        }
    }
    
    //////////////// Check previous cart data with same store /////////////////
    
    func CheckPrevioutStore() -> Bool {
        let cartar = CoredataClass().getCartData().mutableCopy() as! NSArray
        if cartar.count>0 {
            let cart = cartar.object(at: 0) as! Cart
            if cart.store_id != product.store_id {
                return false
            }
        }
        return true
    }
    
    
    
    ////////////// update Item Quantity //////////
    func updateItemQuentity() -> Void {
        let cartarray = CoredataClass().getCartData() as NSArray
        cartLbl.text =  "\(UInt(cartarray.count))"
        
    }
    
    
    @IBAction func ChangeQuentity() -> Void {
        window = UIApplication.shared.keyWindow
        let singleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap))
        singleFingerTap.delegate=self
        window.addGestureRecognizer(singleFingerTap)
        view1=UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width, height: self.view.frame.size.height))
        blackOverlay=UIView(frame: view1.frame)
        blackOverlay.layer.backgroundColor = UIColor.black.cgColor
        blackOverlay.layer.opacity = 0
        view1.addSubview(blackOverlay)
        window.addSubview(view1)
        
        quentityView = UIView(frame: CGRect(x: 25, y: self.view.frame.size.height, width: self.view.frame.size.width-50, height: self.view.frame.size.height/5))
        quentityView.backgroundColor=UIColor.white
        quentityView.layer.cornerRadius=5
        quentityView.layer.borderColor=UIColor.blue.cgColor
        quentityView.layer.borderWidth=2
        view1.addSubview(quentityView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.quentityView.frame.origin.y=self.view.frame.size.height/2-(self.view.frame.size.height/5)/2
            },completion: { (finished: Bool) -> Void in
        })
        
        let titlelabel = UILabel(frame: CGRect(x: 10, y: 5, width: quentityView.frame.size.width-20, height: 25))
        titlelabel.text = "Quantity"
        titlelabel.textAlignment = NSTextAlignment.center
        quentityView.addSubview(titlelabel)
        
        let line = UILabel(frame: CGRect(x: 0, y: titlelabel.frame.size.height+5, width: quentityView.frame.size.width, height: 1))
        line.backgroundColor = UIColor.lightGray
        quentityView.addSubview(line)
        self.btnDesign()
        
    }
    
    func btnDesign() -> Void {
        let quintityBtnview = UIView(frame: CGRect(x: 10, y: 30, width: quentityView.frame.size.width-20, height: quentityView.frame.size.height-40))
        quintityBtnview.tag=1000
        quentityView.addSubview(quintityBtnview)
        var MainQuentity : Int = 0
        if product.product_quantity != nil {
            MainQuentity = Int(product.product_quantity!)!
        }
        print(MainQuentity)
        var firstQty = 0
        var secondQty = 0
        if MainQuentity>5 {
            firstQty = 5
            secondQty = MainQuentity-5
            if secondQty > 5 {
                secondQty = 5
            }
        }
        else{
            firstQty = MainQuentity
        }
        
        if firstQty>0 {
            
            for i in 1...firstQty {
                let btn = UIButton(frame: CGRect(x: CGFloat(i-1)*quintityBtnview.frame.size.width/5+(quintityBtnview.frame.size.width/5)/4, y: quintityBtnview.frame.size.height/8, width: quintityBtnview.frame.size.width/10, height: quintityBtnview.frame.size.width/10))
                btn.setTitle("\(i)", for: .normal)
                btn.setTitleColor(UIColor.black, for: .normal)
                btn.titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
                btn.addTarget(self, action: #selector(self.QuintityBtnAction), for: .touchUpInside)
                btn.layer.cornerRadius = btn.frame.size.height/2
                btn.layer.borderWidth = 1
                btn.layer.borderColor = UIColor.lightGray.cgColor
                btn.tag = i
                if selectedQuantity == i {
                    btn.backgroundColor = UIColor.blue
                    btn.setTitleColor(UIColor.white, for: .normal)
                }
                quintityBtnview.addSubview(btn)
            }
        }
        
        
        if secondQty>0 {
            
            for i in 1...secondQty {
                let btn = UIButton(frame: CGRect(x: CGFloat(i-1)*quintityBtnview.frame.size.width/5+(quintityBtnview.frame.size.width/5)/4, y: (quintityBtnview.frame.size.height/8)*5, width: quintityBtnview.frame.size.width/10, height: quintityBtnview.frame.size.width/10))
                btn.setTitle("\(i+5)", for: .normal)
                btn.titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
                btn.setTitleColor(UIColor.black, for: .normal)
                btn.addTarget(self, action: #selector(self.QuintityBtnAction), for: .touchUpInside)
                btn.layer.cornerRadius = btn.frame.size.height/2
                btn.layer.borderWidth = 1
                btn.layer.borderColor = UIColor.lightGray.cgColor
                btn.tag = i+5
                if selectedQuantity == i+5 {
                    btn.backgroundColor = UIColor.blue
                    btn.setTitleColor(UIColor.white, for: .normal)
                }
                quintityBtnview.addSubview(btn)
            }
            
        }
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if gestureRecognizer == singleTap {
            return true
        }
        else{
            return (touch.view == blackOverlay)
        }
    }
    
    func handleSingleTap() -> Void {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0
            self.quentityView.frame.origin.y=self.view.frame.size.height
            self.view1.endEditing(true)
            },completion: { (finished: Bool) -> Void in
                self.view1.isHidden=true
        })
    }
    
    func QuintityBtnAction(_ sender : UIButton) -> Void {
        selectedQuantity = sender.tag
        quintityLbl.text = "\("Qty: ")\(UInt(sender.tag))"
        sender.backgroundColor = UIColor.blue
        sender.setTitleColor(UIColor.white, for: .normal)
        let subViews = quentityView.subviews
        for subview in subViews{
            if subview.tag == 1000 {
                subview.removeFromSuperview()
            }
        }
        self.btnDesign()
        //        self.handleSingleTap()
    }
    
    
    @IBAction func productDetailMethod() -> Void{
        productDetailBtn.setTitleColor(UIColor.blue, for: .normal)
        userGuideBtn.setTitleColor(UIColor.black, for: .normal)
        buyingBtn.setTitleColor(UIColor.black, for: .normal)
        suppInfoBtn.setTitleColor(UIColor.black, for: .normal)
        productDetailLbl.isHidden = false
        userGuideLbl.isHidden = true
        buyingLbl.isHidden = true
        suppInfoLbl.isHidden = true
        descriptionTextview.text = product.product_description
        
    }
    
    
    @IBAction func UserGuideMethod() -> Void{
        productDetailBtn.setTitleColor(UIColor.black, for: .normal)
        userGuideBtn.setTitleColor(UIColor.blue, for: .normal)
        buyingBtn.setTitleColor(UIColor.black, for: .normal)
        suppInfoBtn.setTitleColor(UIColor.black, for: .normal)
        productDetailLbl.isHidden = true
        userGuideLbl.isHidden = false
        buyingLbl.isHidden = true
        suppInfoLbl.isHidden = true
        descriptionTextview.text = product.user_guide
    }
    
    
    @IBAction func buyingMethod() -> Void{
        productDetailBtn.setTitleColor(UIColor.black, for: .normal)
        userGuideBtn.setTitleColor(UIColor.black, for: .normal)
        buyingBtn.setTitleColor(UIColor.blue, for: .normal)
        suppInfoBtn.setTitleColor(UIColor.black, for: .normal)
        productDetailLbl.isHidden = true
        userGuideLbl.isHidden = true
        buyingLbl.isHidden = false
        suppInfoLbl.isHidden = true
        descriptionTextview.text = product.buying_guide
    }
    
    
    @IBAction func suppInfoMethod() -> Void{
        productDetailBtn.setTitleColor(UIColor.black, for: .normal)
        userGuideBtn.setTitleColor(UIColor.black, for: .normal)
        buyingBtn.setTitleColor(UIColor.black, for: .normal)
        suppInfoBtn.setTitleColor(UIColor.blue, for: .normal)
        productDetailLbl.isHidden = true
        userGuideLbl.isHidden = true
        buyingLbl.isHidden = true
        suppInfoLbl.isHidden = false
        descriptionTextview.text = product.supplement_info
    }
    
    @IBAction func LikeButtonMethod(_ sender : UIButton) -> Void{
        
        if (sender.isEqual(heartBtn))  {
            
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                self.RedheartBtn.frame = self.heartFrame1
                }, completion: nil)
            
            
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            
            let List = NSEntityDescription.insertNewObject(forEntityName: "MyList", into: moc) as! Product
            List.price = product.price
            List.product_id = product.product_id
            List.product_name = product.product_name
            List.product_name_ar = product.product_name_ar
            List.product_description = product.product_description
            List.product_description_ar = product.product_description_ar
            List.product_image = product.product_image
            List.store_id = product.store_id
            List.product_quantity = product.product_quantity
            List.buying_guide = product.buying_guide
            List.supplement_info = product.supplement_info
            List.user_guide = product.user_guide
            List.rating = product.rating
            List.store_name = product.store_name
            List.store_name_ar = product.store_name_ar

            do {
                try moc.save()
            } catch {
                print(error)
            }
        }
        else{
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                self.RedheartBtn.frame = self.heartFrame0
                }, completion: nil)
            let listItems = CoredataClass().GetMyListItemWithProductID(product.product_id!)
            if listItems.count > 0 {
                let moc = (UIApplication.shared.delegate
                    as! AppDelegate).persistentContainer.viewContext
                
                let listitem = listItems.object(at: 0) as! Product
                moc.delete(listitem)
                do {
                    try moc.save()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setCartRIghtbarbutton()
    }
    
    func setCartRIghtbarbutton() -> Void {
        let cartarray = CoredataClass().getCartData() as NSArray
        
        let cartBtn = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(40), height: CGFloat(40)))
        cartBtn.setImage(UIImage(named: "cart_icon")!, for: .normal)
        cartBtn.addTarget(self, action: #selector(self.CartbuttonItem), for: .touchUpInside)
        if cartarray.count > 0 {
            let cartitelbl = UILabel(frame: CGRect(x: CGFloat(30), y: CGFloat(2), width: CGFloat(15), height: CGFloat(8)))
            cartitelbl.font = UIFont.systemFont(ofSize: 8)
            cartitelbl.backgroundColor = UIColor.white
            cartitelbl.clipsToBounds = true
            cartitelbl.layer.cornerRadius = cartitelbl.frame.size.height / 2
            cartitelbl.textAlignment = .center
            cartitelbl.textColor = UIColor.blue
            cartitelbl.text = "\(UInt(cartarray.count))"
            cartBtn.addSubview(cartitelbl)
        }
        let cartbtn = UIBarButtonItem(customView: cartBtn)
        self.navigationItem.rightBarButtonItems = [cartbtn]
    }
    
    @IBAction func CartbuttonItem() -> Void {
        
        if self.tabBarController != nil {
            self.tabBarController?.selectedIndex = 2
        }
        else{
            UserDefaults.standard.set(2, forKey: SelectedTab)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "revalvc") as! SWRevealViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.set(rootViewController: vc)
        }
    }
    
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)
        
    }
    
    func scrollTapgesture(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.custumSizeView.isHidden = true
            },completion: { (finished: Bool) -> Void in
        })
        
    }
    
    
    @IBAction func sizeBtnMethod() -> Void{
        
        custumSizeView = UIView(frame: CGRect(x: sizeView.frame.origin.x+sizeView.frame.size.width , y: sizeView.frame.origin.y, width: 0, height: 0))
        scrollView.addSubview(custumSizeView)
        
        let frame = sizeView.frame
        custumSizeView.backgroundColor = UIColor.white
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.custumSizeView.frame = CGRect(x: frame.origin.x-frame.size.width/2 , y: frame.origin.y, width: frame.size.width+frame.size.width/2, height: 40*CGFloat(self.sizeArr.count))
            
            },completion: { (finished: Bool) -> Void in
                self.sizeCustumview()
        })
        
    }
    
    func  sizeCustumview() -> Void {
        
        custumSizeView.layer.shadowColor = UIColor.gray.cgColor
        custumSizeView.layer.shadowOffset = CGSize(width: CGFloat(3.0), height: CGFloat(3.0))
        custumSizeView.layer.shadowOpacity = 0.7
        custumSizeView.layer.shadowRadius = 3
        
        let frame1 = custumSizeView.frame
        let height : CGFloat = 40
        
        for a in 0..<sizeArr.count {
            
            let sizedic = sizeArr.object(at: a) as! NSDictionary
            
            let width = frame1.size.width/10
            let sizeInnerVIew = UIView(frame: CGRect(x: 0, y: CGFloat(a)*height, width: frame1.size.width, height: height))
            custumSizeView.addSubview(sizeInnerVIew)
            let sizelbl = UILabel(frame: CGRect(x: 5, y: 0 , width: width*6, height: height-1))
            sizelbl.text = sizedic.object(forKey: "sizename") as? String
            sizelbl.font = UIFont.systemFont(ofSize: 14)
            sizeInnerVIew.addSubview(sizelbl)
            
            let pricelbl = UILabel(frame: CGRect(x: width*6, y: 0, width: width*4-5, height: height-1))
            pricelbl.text = "KD \(sizedic.object(forKey: "price") as! String)"
            pricelbl.font = UIFont.systemFont(ofSize: 14)
            pricelbl.textAlignment = NSTextAlignment.right
            sizeInnerVIew.addSubview(pricelbl)
            
            let line = UILabel(frame: CGRect(x: 0, y: height-1, width: custumSizeView.frame.size.width, height: 1))
            line.backgroundColor = UIColor.init(red: 210/255, green: 210/255, blue: 210/255, alpha: 1)
            sizeInnerVIew.addSubview(line)
            
            let sizebtn = UIButton(frame: CGRect(x: 0, y: 0, width: custumSizeView.frame.size.width, height: height))
            sizebtn.tag = a
            sizebtn.addTarget(self, action: #selector(self.SizeMethod), for: .touchUpInside)
            sizeInnerVIew.addSubview(sizebtn)
            
        }
    }
    
    func SizeMethod(_ sender : UIButton) -> Void {
        let sizedic = sizeArr.object(at: sender.tag) as! NSDictionary
        productPriceLbl.text = "KD \(sizedic.object(forKey: "price") as! String)"
        productPrice = NSString(string: sizedic.object(forKey: "price") as! String).floatValue
        sizeLbl.text = sizedic.object(forKey: "sizename") as? String
        custumSizeView.isHidden = true
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
