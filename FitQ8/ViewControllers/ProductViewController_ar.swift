//
//  ProductViewController_ar.swift
//  FitQ8
//
//  Created by Manish Kumar on 18/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Alamofire

class ProductViewController_ar: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate {
    
    var store : Store!
    var category : Category!
    
    
    
    @IBOutlet var ProductTabel : UITableView!
    var  ProductList = NSMutableArray()
    var searchTable : UITableView!
    var filterarray = NSMutableArray()
    var searchBarEdition = false
    @IBOutlet var searchBar : UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        if category != nil {
            self.navigationItem.title = category.category_name
        }
        else{
            self.navigationItem.title = store.store_name
        }
        
        ProductTabel.register(UINib(nibName: "Productcell_ar", bundle: nil), forCellReuseIdentifier: "cell")
        
        searchTable = UITableView()
        searchTable.register(UINib(nibName: "searchCell", bundle: nil), forCellReuseIdentifier: "searchCell")
        // Do any additional setup after loading the view.
        
        self .GetProduct()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.bool(forKey: ratesuccess) {
            UserDefaults.standard.set(false, forKey: ratesuccess)
            self.GetProduct()
        }
        
        let cartarray = CoredataClass().getCartData() as NSArray
        
        let cartBtn = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(40), height: CGFloat(40)))
        cartBtn.setImage(UIImage(named: "cart_icon")!, for: .normal)
        cartBtn.addTarget(self, action: #selector(self.CartbuttonItem), for: .touchUpInside)
        if cartarray.count > 0 {
            let cartitelbl = UILabel(frame: CGRect(x: CGFloat(30), y: CGFloat(2), width: CGFloat(15), height: CGFloat(8)))
            cartitelbl.font = UIFont.systemFont(ofSize: 8)
            cartitelbl.backgroundColor = UIColor.white
            cartitelbl.clipsToBounds = true
            cartitelbl.layer.cornerRadius = cartitelbl.frame.size.height / 2
            cartitelbl.textAlignment = .center
            cartitelbl.textColor = UIColor.blue
            cartitelbl.text = "\(UInt(cartarray.count))"
            cartBtn.addSubview(cartitelbl)
        }
        let cartbtn = UIBarButtonItem(customView: cartBtn)
        
        let btn2 = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(40), height: CGFloat(40)))
        btn2.setImage(UIImage(named: "search"), for: .normal)
        btn2.addTarget(self, action: #selector(self.searchmethod), for: .touchUpInside)
        let item2 = UIBarButtonItem()
        item2.customView = btn2
        
        self.navigationItem.rightBarButtonItems = [item2,cartbtn]
        
    }
    
    func CartbuttonItem() -> Void{
        
        self.tabBarController?.selectedIndex = 2
    }
    
    
    func GetProduct() -> Void {
        if  Utility().hasInternetConnection() {
            let moc = (UIApplication.shared.delegate
                as! AppDelegate).persistentContainer.viewContext
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true) as MBProgressHUD
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = loading_ar
            self.view.addSubview(hud)
            var  parameters = Parameters()
            var url = String()
            if category != nil {
                parameters = ["apikey":API_KEY,
                              "category_id":category.category_id! as String ]
                url = CAT_PRODCUT_URL
            }
            else{
                parameters = ["apikey":API_KEY,
                              "storeid":store.store_id! as String
                               ]
                url = STORE_PRODUCT
            }
        
            
            Alamofire.request(url, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    hud.removeFromSuperview()
                    
                    let jsonObject = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(JKParseOptionLooseUnicode))) as! NSDictionary
                    print(jsonObject)
                    
                    if (jsonObject.object(forKey: "status") as! Bool) == true {
                        CoredataClass().DeleteAllData("Product")
                        let dataAr = NSMutableArray()
                        let dicEntry = jsonObject.object(forKey: "data") as! NSArray
                        for i in 0..<dicEntry.count  {
                            let dictCat = dicEntry.object(at: i) as! NSDictionary
                            print(dictCat)
                            
                            let className = NSStringFromClass(Product.self) as String
                            let entity = NSEntityDescription.entity(forEntityName:className, in: moc)!
                            let cat = (NSManagedObject(entity: entity, insertInto: moc) as! Product)
                            cat.safeSetValuesForKeys(with: dictCat as Any? as? [AnyHashable: Any] ?? [:] )
                            dataAr.add(cat)
                            do {
                                try moc.save()
                            } catch {
                                print(error)
                            }
                        }
                        print(dataAr)
                        self.ProductList = CoredataClass().getProductData().mutableCopy() as! NSMutableArray
                        self.ProductTabel.delegate=self
                        self.ProductTabel.dataSource=self
                        self.ProductTabel.reloadData()
                    }
                    else{
                        let alert = DXAlertView(title: nil, contentText: jsonObject.object(forKey: "message") as? String, leftButtonTitle: nil, rightButtonTitle: "OK")
                        alert?.show()
                        alert?.rightBlock = {() -> Void in
                        }
                    }

                case .failure(let error):
                    hud.removeFromSuperview()
                    print("Request failed with error: \(error)")
                    let alert = DXAlertView(title: nil, contentText: Connection_Failed_ar, leftButtonTitle: "Cancel", rightButtonTitle: "Retry")
                    alert?.show()
                    alert?.rightBlock = {() -> Void in
                        self.GetProduct()
                    }
                }
                
            })
        }
        else{
            
            let alert = DXAlertView(title: nil, contentText: check_internet_ar,
            leftButtonTitle: nil, rightButtonTitle: "Ok")
            alert?.show()
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBarEdition  {
            return self.filterarray.count
        }
        return self.ProductList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        if !searchBarEdition {
            let cell:CustumCell = ProductTabel.dequeueReusableCell(withIdentifier: "cell") as! CustumCell!
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.backView?.layer.shadowColor = UIColor.gray.cgColor
            cell.backView?.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(1.0))
            cell.backView?.layer.shadowOpacity = 0.7
            cell.backView?.layer.shadowRadius = 1
            
            let product = ProductList.object(at: indexPath.row) as! Product
            
            cell.storeName?.text = product.product_name_ar
            cell.storeName?.numberOfLines = 0
            cell.store_des?.text = product.product_description_ar
            cell.store_des?.numberOfLines = 0
            cell.cat_store?.isHidden = true
            cell.cat_storeBtn?.isHidden = true
            if category != nil {
                cell.cat_store?.isHidden  = false
                cell.cat_store?.text = product.store_name_ar
                cell.cat_store?.textColor = UIColor.blue
                cell.cat_storeBtn?.isHidden = false
                cell.cat_storeBtn?.addTarget(self, action: #selector(self.showStore_product(_:)), for: .touchUpInside)
            }
            cell.storeImage?.setimage(link: product.product_image)
            
            cell.Rateview?.notSelectedImage = UIImage.init(named: star_empty)
            cell.Rateview?.halfSelectedImage = UIImage.init(named: star_Half)
            cell.Rateview?.fullSelectedImage = UIImage.init(named: star_Full)
            cell.Rateview?.editable = false;
            cell.Rateview?.maxRating = 5;
            cell.Rateview?.midMargin = 0;
            cell.Rateview?.isUserInteractionEnabled = false;
            var rating : Float = 0.0
            if product.rating?.isEmpty == false {
                let datadic = (product.rating!).objectFromJSONString() as! NSDictionary
                let totalRatingCount = (datadic.object(forKey: "totalstar") as! NSString).floatValue
                let baseRating = (datadic.object(forKey: "totalrecord") as! NSString).floatValue
                rating = totalRatingCount/baseRating
            }
            cell.Rateview?.rating = rating
            cell.RateButton?.addTarget(self, action: #selector(self.didClickRateButton), for: .touchUpInside)
            
            
            return cell
        }
        else{
            let cell:CustumCell = searchTable.dequeueReusableCell(withIdentifier: "searchCell") as! CustumCell!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            let product = filterarray.object(at: indexPath.row) as! Product
            cell.textLabel?.text = product.product_name
            
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        //        self.performSegue(withIdentifier: "productdetail", sender: self)
        let productDetail = self.storyboard?.instantiateViewController(withIdentifier: "productdetail") as! ProductDetailVC_ar
        if searchBarEdition {
            productDetail.product = filterarray.object(at: indexPath.row) as! Product
        }
        else{
            productDetail.product = ProductList.object(at: indexPath.row) as! Product
        }
        self.navigationController?.pushViewController(productDetail, animated: true)
    }
    
    
    /////////////  Did click button Rate ////////
    func didClickRateButton(_ sender : UIButton) -> Void  {
        
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: ProductTabel)
        let indexPath = ProductTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "rateNav") as! UINavigationController
        Variables.product = ProductList.object(at: indexPath.row) as? Product
        Variables.Is_Store = false
        self.present(vc, animated: true, completion: nil)
    }
    
    
    ///////////// show to store products ///////////////
    func showStore_product(_ sender : UIButton) -> Void {
        let center = sender.center
        let  rootViewPoint = sender.superview?.convert(center, to: ProductTabel)
        let indexPath = ProductTabel.indexPathForRow(at: rootViewPoint!)!
        print("\(indexPath)")
        
        let product = ProductList.object(at: indexPath.row) as! Product
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let store1 = NSEntityDescription.insertNewObject(forEntityName: "Store", into: moc) as! Store
        store1.store_id = product.store_id
        store1.store_name = product.store_name
        store1.store_name_ar = product.store_name_ar
        category = nil
        store = store1
        self.GetProduct()
        self.navigationItem.title = product.store_name_ar
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchDisplayController?.setActive(false, animated: false)
    }
    
    func searchmethod() -> Void {
        searchBar.becomeFirstResponder()
        if searchBarEdition == false {
            searchBarEdition = true
        }
    }
    
    ////////////////// Search bar delegate method///////
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("Begin")
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("End")
        searchBarEdition = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("cancel")
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Change")
        filterarray.removeAllObjects()
        filterarray = CoredataClass().getProductSearchData(searchText).mutableCopy() as! NSMutableArray
        print(filterarray)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func BackButton() -> Void {
        
        self.navigationController!.popViewController(animated: true)

    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
