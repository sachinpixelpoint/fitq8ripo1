//
//  ConfigFile.swift
//  FitQ8
//
//  Created by Manish Kumar on 29/12/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import Foundation


///////////// STRINGS ///////////////
let SelectedTab = "selectedTab"
let tabCondition = "TabCondition"

let UserID = "UserId"
let UserName = "UserName"
let Password = "Password"
let EmailId = "emailId"
let Phone_no = "phone_no"
let Gender = "Gender"
let User_photo = "user_Photo"
let Notification_on = "notification"
let lang = "language"
let firstLaunch = "first_launch"
let english = "English"
let arabic = "Arabic"
let area_id = "Area_id"
let ratesuccess = "Ratesuccess"


/////// API KEY/////////
let API_KEY = "fitq"

/////////////////// BASE URL /////////////////////
let BASE_URL = "http://fitq8.com/webservice/"

/////////// STORE URL/////////////
let STORE_URL =  "\(BASE_URL)\("store")"

//////////// Store_data_url //////////
let STORE_DATA =  "\(BASE_URL)\("storedata")"

//////////// Store_data_url //////////
let STORE_PRODUCT =  "\(BASE_URL)\("storeproduct")"

//////////// SIGN_IN_URL//////
let SIGN_IN_URL =  "\(BASE_URL)\("login")"

///////////SIGN_UP_URL //////
let SIGNUP_URL =  "\(BASE_URL)\("signup")"

/////////FORGOT_URL//////
let FORGOT_PASS_URL =  "\(BASE_URL)\("forgotpassword")"

///////////  CHECK PASSWORD CODE ///////////
let CHECK_CODE = "\(BASE_URL)\("checkcode")"

/////////// UPDATE PASSWORD ///////////
let UPDATE_PASSWORD = "\(BASE_URL)\("updatepassword")"

/////////// CHANGE PASSWORD ///////////
let CHANGE_PASS = "\(BASE_URL)\("changepassword")"


//////////// CATEGORY URL ///////////////
let CAT_URL = "\(BASE_URL)\("allcategory")"

//////////// CATEGORY PRODUCT URL ///////////////
let CAT_PRODCUT_URL = "\(BASE_URL)\("categoryproduct")"

/////////ADD ADDRESS URL//////
let SAVE_ADD_URL = "\(BASE_URL)\("addaddress")"

///// UPDATE ADDRESS ///
let GET_ADD_URL = "\(BASE_URL)\("getaddress")"

//////// DELETE_ADDRESS //////
let DELETE_ADD_URL = "\(BASE_URL)\("deleteaddress")"

//////// UPDATE ADDRESS URL //////
let UPDATE_ADD_URL = "\(BASE_URL)\("updateaddress")"

//////// PLACE ORDER URL //////
let ORDER_PLACE = "\(BASE_URL)\("addorder")"

/////// GET AREA//////////////////
let Get_Area_URL = "\(BASE_URL)\("area")"

//////// GET ORDERS /////////////////
let Get_Orders_URL = "\(BASE_URL)\("myorder")"

//////// ADD PRODUCT RATING /////////////////
let ADD_PRODUCT_RATING = "\(BASE_URL)\("addproductrating")"

//////// ADD STORE RATING /////////////////
let ADD_STORE_RATING = "\(BASE_URL)\("addstorerating")"

//////// CANCEL ORDER /////////////////
let CANCEL_ORDER = "\(BASE_URL)\("cancelorder")"

//////// Get delivery price /////////////////
let DELIVERY_PRICE = "\(BASE_URL)\("deliveryprice")"



//// images/////
let placeHolderImg = "thumb_img"

let star_Full = "Star_gold_full"
let star_Full1 = "star_gold_full1"
let star_Half = "Star_gold_half"
let star_empty = "Star_gold_empty"
let star_empty1 = "Star_empty1"


/////////////COMMOM Colors//////////
let Theme_blue_Color = UIColor.init(red: 16/255, green: 0/255, blue: 182/255, alpha: 1)
let  darkgreen = UIColor.init(red: 90/255, green: 155/255, blue: 0/255, alpha: 1)
let green = UIColor.init(red: 146/255, green: 198/255, blue: 0/255, alpha: 1)
let darkyellow = UIColor.init(red: 237/255, green: 148/255, blue: 0/255, alpha: 1)
let yellow = UIColor.init(red: 237/255, green: 184/255, blue: 0/255, alpha: 1)
let red = UIColor.init(red: 172/255, green: 29/255, blue: 32/255, alpha: 1)


/////////////////// Aleart strings ///////////////
let loading = "Loading..."
let Connection_Failed = "Connection Failed"
let check_internet = "Please check your Internet connect and try again"
let No_record = "No Record Found"


let loading_ar = "تحميل …."
let Connection_Failed_ar = "فشل الاتصال"
let check_internet_ar = "الرجاء التحقق من الاتصال بالانترنت"
let No_record_ar = "لا يوجد سجلات"




////////////////  Notification strings ////////////////
//let  CONNECTION_FAILED = "Connection failed try again"

