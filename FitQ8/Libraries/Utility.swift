//
//  Utility.swift
//  FitQ8
//
//  Created by Manish Kumar on 24/12/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class Utility: NSObject {
    
    func hasInternetConnection() -> Bool {
        
        var reachability = Reachability()
        reachability = Reachability.forInternetConnection()
        let internetStatus = reachability.currentReachabilityStatus()
        if internetStatus != NotReachable {
            return true
        }
        
        return false
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = color
    }
    
    func AddSidePanel(VC: UIViewController){
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu_1"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(VC.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1) as UIBarButtonItem
        VC.navigationItem.leftBarButtonItem = item1;
        VC.view.addGestureRecognizer(VC.revealViewController().panGestureRecognizer())
        VC.view.addGestureRecognizer(VC.revealViewController().tapGestureRecognizer())
    }
}

extension UIImageView {
    
    func setimage(link : String?){
        if link != nil && link != ""{
            let url = URL(string: link!)
            self.af_setImage(withURL: url!, placeholderImage: UIImage.init(named: placeHolderImg), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: true, completion: nil)
        }
        else{
            self.image = UIImage.init(named: placeHolderImg)
        }
    }
    
    func setimageWithout_PH(link : String?){
        if link != nil && link != ""{
            let url = URL(string: link!)
            self.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: true, completion: nil)
        }
    }
}

extension UIImage {
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

struct Variables {
    static var product : Product!
    static var store : Store!
    static var Is_Store = false
}
