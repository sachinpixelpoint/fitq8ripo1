//
//  UserSession.swift
//  FitQ8
//
//  Created by Manish Kumar on 05/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

@objc(UserSession)
class UserSession: NSObject {
    
    var userId : String?
    var userName : String?
    var PhotoUrl : String?
    var email : String?
    var password : String?
    var phoneNo : String?
    var gender : String?
    
    
    func getUserSession() -> UserSession {
        let usersession = UserSession()
        usersession.userId = UserDefaults.standard.object(forKey: UserID) as? String
        usersession.email = UserDefaults.standard.object(forKey: EmailId) as? String
        usersession.phoneNo = UserDefaults.standard.object(forKey: Phone_no) as? String
        usersession.gender = UserDefaults.standard.object(forKey: Gender) as? String
        usersession.userName = UserDefaults.standard.object(forKey: UserName) as? String
        usersession.PhotoUrl = UserDefaults.standard.object(forKey: User_photo) as? String
        usersession.password = UserDefaults.standard.object(forKey: Password) as? String
        print(usersession.userId)
        print(usersession)
        return usersession
    }

    
    func SetUserSession(_ usersession : UserSession) -> Void {
        print(usersession)
        print(usersession.userId)
        UserDefaults.standard.set(usersession.userId, forKey: UserID)
        UserDefaults.standard.set(usersession.email, forKey: EmailId)
        UserDefaults.standard.set(usersession.phoneNo, forKey: Phone_no)
        UserDefaults.standard.set(usersession.gender, forKey: Gender)
        UserDefaults.standard.set(usersession.userName, forKey: UserName)
        UserDefaults.standard.set(usersession.PhotoUrl, forKey: User_photo)
        UserDefaults.standard.set(usersession.password, forKey: Password)
        
    }
    
    func clearUserSession() -> Void {
        UserDefaults.standard.set(nil, forKey: UserID)
        UserDefaults.standard.set(nil, forKey: EmailId)
        UserDefaults.standard.set(nil, forKey: Phone_no)
        UserDefaults.standard.set(nil, forKey: Gender)
        UserDefaults.standard.set(nil, forKey: UserName)
        UserDefaults.standard.set(nil, forKey: User_photo)
        UserDefaults.standard.set(nil, forKey: Password)
        UserDefaults.standard.set(nil, forKey: UserID)
    }
    
}
