//
//  Category.swift
//  FitQ8
//
//  Created by Manish Kumar on 10/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//
import UIKit
import CoreData
import Foundation

@objc(Category)
public class Category: NSManagedObject {
    
    @NSManaged var category_name : String?
    @NSManaged var category_name_ar : String?
    @NSManaged var category_id : String?
    @NSManaged var parent_id : String?

}
