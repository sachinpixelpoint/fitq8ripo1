//
//  Product.swift
//  FitQ8
//
//  Created by Manish Kumar on 29/12/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit
import CoreData
import Foundation

@objc(Product)
public class Product: NSManagedObject {
    
    @NSManaged var product_id : String?
    @NSManaged var product_name : String?
    @NSManaged var product_name_ar : String?
    @NSManaged var product_description : String?
    @NSManaged var product_description_ar : String?
    @NSManaged var product_image : String?
    @NSManaged var product_quantity : String?
    @NSManaged var store_id : String?
    @NSManaged var price : String?
    @NSManaged var buying_guide : String?
    @NSManaged var supplement_info : String?
    @NSManaged var user_guide : String?
    @NSManaged var rating : String?
    @NSManaged var store_name : String?
    @NSManaged var store_name_ar : String?
    
    
}
