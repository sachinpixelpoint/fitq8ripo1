//
//  Cart.swift
//  FitQ8
//
//  Created by Manish Kumar on 02/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import CoreData
import Foundation


@objc(Cart)
public class Cart: NSManagedObject {
    
    @NSManaged var product_id : String?
    @NSManaged var product_image : String?
    @NSManaged var product_name : String?
    @NSManaged var product_name_ar : String?
    @NSManaged var product_des : String?
    @NSManaged var quentity : String?
    @NSManaged var store_id : String?
    @NSManaged var price : String?
    @NSManaged var rating : String?
    @NSManaged var product_quantity : String?
    @NSManaged var delivery_price : String?
    @NSManaged var min_order : String?

}
