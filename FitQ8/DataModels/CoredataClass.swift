//
//  CoredataClass.swift
//  FitQ8
//
//  Created by Manish Kumar on 02/01/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class CoredataClass: NSObject {
    
    
    ////////////// Get Cart data ///////////
    func getCartData() -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart" )
        fetchRequest.returnsObjectsAsFaults = false
        let entity = NSEntityDescription.entity(forEntityName: "Cart", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
     ////////////// Get store all data /////
    func getStoreData() -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Store" )
        fetchRequest.returnsObjectsAsFaults = false
        let entity = NSEntityDescription.entity(forEntityName: "Store", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    
    }
    
        ////////////// Get store Data acording to search text /////
        func getStoreSearchData(_ searchText : String) -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Store" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "store_name contains[c] %@", searchText)
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "Store", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    ///////////// Get store  acroding to store id /////////
    func getStorewithStoreID(_ storeid : String) -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Store" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "store_id = %@", storeid)
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "Store", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    ///////////// Delete all data from Entity
    func DeleteAllData(_ entityName : String) -> Void {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName )
        fetchRequest.returnsObjectsAsFaults = false
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        for item in items {
            moc.delete(item as! NSManagedObject)
            do {
                try moc.save()
            } catch {
                print(error)
            }
        }
    }
    
    ///////////////  Get Product all data ///////////
    func getProductData() -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Product" )
        fetchRequest.returnsObjectsAsFaults = false
        let entity = NSEntityDescription.entity(forEntityName: "Product", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    ///////////// Get product serch data acroding to search text /////////
    func getProductSearchData(_ searchText : String) -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Product" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "product_name contains[c] %@", searchText)
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "Product", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    ////////////// Get category all data /////
    func getCategoryData() -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Category" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "parent_id = 0")
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "Category", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    ////////////// Get category Item data /////
    func getCategoryItemData(_ categoryID : String) -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Category" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "parent_id = %@",categoryID)
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "Category", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    

    ////////////// Get Address all data /////
    func getAddressData() -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Address" )
        fetchRequest.returnsObjectsAsFaults = false
        let entity = NSEntityDescription.entity(forEntityName: "Address", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    //////////////// Get area 
    func getAreaData() -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Area" )
        fetchRequest.returnsObjectsAsFaults = false
        let entity = NSEntityDescription.entity(forEntityName: "Area", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    ///////////// Get Area serch data acroding to search text /////////
    func getAreaSearchData(_ searchText : String) -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Area" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "area_name contains[c] %@ or area_name_ar contains[c] %@", searchText,searchText)
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "Area", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
    }
    
    ///////////// Get Area  acroding to area id /////////
    func getAreaFromAreaId(_ areaid : String) -> Area {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Area" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "area_id = %@", areaid)
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "Area", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest) as NSArray
        print(items)
        var area : Area
        area = items.object(at: 0) as! Area
        return area
        
    }
    
    ///////////// GET MY LIST DATA /////////
    func GetMyList() -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MyList" )
        fetchRequest.returnsObjectsAsFaults = false
        let entity = NSEntityDescription.entity(forEntityName: "MyList", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
        
    }
    
    func GetMyListItemWithProductID(_ product_id : String) -> NSArray {
        let moc = (UIApplication.shared.delegate
            as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MyList" )
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "product_id = %@", product_id)
        fetchRequest.predicate = resultPredicate
        let entity = NSEntityDescription.entity(forEntityName: "MyList", in: moc)!
        fetchRequest.entity = entity
        let items = try! moc.fetch(fetchRequest)
        print(items)
        return items as NSArray
        
    }
    
}
