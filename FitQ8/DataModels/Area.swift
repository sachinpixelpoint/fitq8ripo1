//
//  Area.swift
//  FitQ8
//
//  Created by Manish Kumar on 1/16/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import CoreData
import Foundation

@objc (Area)
class Area: NSManagedObject {

    @NSManaged var area_id : String?
    @NSManaged var area_name : String?
    @NSManaged var area_name_ar : String?
    @NSManaged var delivery_charge : String?
}
