//
//  Address.swift
//  FitQ8
//
//  Created by Manish Kumar on 1/11/17.
//  Copyright © 2017 Manish Kumar. All rights reserved.
//

import UIKit
import CoreData
import Foundation

@objc(Address)
public class Address: NSManagedObject {

    
    @NSManaged var address_id : String?
    @NSManaged var address_name : String?
    @NSManaged var area_id : String?
    @NSManaged var block : String?
    @NSManaged var extra_delivery_information : String?
    @NSManaged var floor : String?
    @NSManaged var jadda : String?
    @NSManaged var lat : String?
    @NSManaged var lon : String?
    @NSManaged var phone : String?
    @NSManaged var street_num : String?
    @NSManaged var house_num : String?

}
