//
//  Store.swift
//  Fitq8
//
//  Created by Manish Kumar on 12/23/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit
import CoreData
import Foundation

@objc(Store)
public class Store: NSManagedObject {
    // your class
    
    @NSManaged var store_id : String?
    @NSManaged var store_name : String?
    @NSManaged var store_image : String?
    @NSManaged var ratingdata : String?
    @NSManaged var store_name_ar : String?
    
}
